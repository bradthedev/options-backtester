/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <QDialog>

namespace Ui {
class aboutwindow;
}

class aboutwindow : public QDialog
{
    Q_OBJECT

public:
    explicit aboutwindow(QWidget *parent = 0);
    ~aboutwindow();

private slots:
    void on_backButton_clicked();

private:
    Ui::aboutwindow *ui;
};

#endif // ABOUTWINDOW_H
