/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "aboutwindow.h"
#include "addrules.h"
#include "backtestermainwindow.h"
#include "databaseloader.h"
#include "reportsmainwindow.h"
#include "strategies.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    //child windows

    aboutwindow aboutwindowMain;
    BacktesterMainWindow backtesterMain;
    DatabaseLoader dbloaderMain;
    ReportsMainWindow reportsMain;
    strategies strategiesMain;

    ~MainWindow();
    
private slots:

    void on_btnReports_clicked();

    void on_btnBacktester_clicked();

    void on_newStragety_triggered();

    void on_actionExit_triggered();

    void on_manual_triggered();

    void on_about_triggered();

private:
    Ui::MainWindow * ui;
};

#endif // MAINWINDOW_H
