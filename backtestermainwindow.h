/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef BACKTESTERMAINWINDOW_H
#define BACKTESTERMAINWINDOW_H

#include <QListWidgetItem>
#include <QWidget>
#include <QStringList>
#include <QDate>

//#include "strategyeditorwindow.h"
#include "database.h"
#include "report.h"
#include "reportwindow.h"
#include "strategychooser.h"
#include "ruleschooser.h"

namespace Ui {
class BacktesterMainWindow;
}

class BacktesterMainWindow : public QWidget
{
    Q_OBJECT
    
public:

    QVector<QStringList> constructIDList;
    QVector<QStringList> expireList;
    QVector<QStringList> daysList;
    QVector<QStringList> strikeList;
    QVector<QStringList> actionList;
    QVector<QStringList> typeList;

    QStringList strats;


    explicit BacktesterMainWindow(QWidget *parent = 0);

    ~BacktesterMainWindow();

    void openWindow();
    void updateComboStrike();
    void updateComboExpire();
    void buyOptions(double &capital, OPTION &option, QString type); //Buys capital amount of market exposure in options
    void loadStrategy(QListWidgetItem *theStrategy);
    void setOffAskUnder(int index, int stratIndex);
    void writeRules(int cid);
    void setConstuctList(const QString &currentText);
    void deleteARule(int rid);
    void deleteAStrategy(int stratRow);

    QString exitRule(OPTION o, QDate startD, QDate curDate, QVector<QStringList> listOfRules);
    bool getResult(int a, QString operat, int b);

    double calcWorth(double money, QDate date, OPTION option);
    int diffMonths(QDate start, QDate end);
    int getCID();
    int CIDget(QString constructString);
    int getRID(QString rule);
    QDate calculateValidExpiry(QString expire, QString strike, int currentIndex, int stratIndex);

    strategychooser stratChooser;
    rulesChooser rulesAdder;
    //StrategyEditorWindow stratEdit;
    QDate startDate, endDate;
    Report report;
    ReportWindow reportWindow;
    double strikePOffset; //percent offset
    Database db;

public slots:
    void onStrategySelected(const QList<QListWidgetItem *> &chosenStrategy);
    void onStrategyDeleted(QString delStrat);
    void onRuleSelected(QString rule);
    void onRuleDeleted(QString delRule);

private slots:
    void on_btnRun_clicked();

    void on_dateStart_userDateChanged(const QDate &date);

    void on_dateEnd_userDateChanged(const QDate &date);

    void on_btnQuit_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_3_clicked();

    void on_listWidget_currentTextChanged(const QString &currentText);

    void on_constructList_itemClicked(QListWidgetItem *item);

    void on_pushButton_6_clicked();

private:
    Ui::BacktesterMainWindow *ui;
};

#endif // BACKTESTERMAINWINDOW_H
