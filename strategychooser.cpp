/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QStandardItem>
#include <QStandardItemModel>
#include <QDebug>

#include "strategychooser.h"
#include "ui_strategychooser.h"

strategychooser::strategychooser(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::strategychooser)
{
    ui->setupUi(this);

    connect(ui->addBtn, SIGNAL(clicked()),this, SLOT(onAddStrategySelectedPressed()));

    connect(ui->pushButton_3, SIGNAL(clicked()),this, SLOT(stratDeleted()));

    QObject::connect(&strat, SIGNAL(newStratCreated()),
                     this, SLOT(onStratAdded()));

    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);
    this->setPalette(pal);
    this->setWindowTitle("Pick a Strategy");
}

strategychooser::~strategychooser()
{
    delete ui;
}

void strategychooser::onStratAdded() {
        clearTable();
        loadStategies();
        ui->strategyList->repaint();
}

void strategychooser::onAddStrategySelectedPressed() {
    emit this->sendStrategy(ui->strategyList->selectedItems());
}

void strategychooser::stratDeleted() {
    emit this->sendDeletedStrategy(weakestLink);
}

void strategychooser::openWindow() {
    clearTable();
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
    loadStategies();
    this->show();
}

void strategychooser::on_createBtn_clicked()
{
    this->strat.openWindow();
}

void strategychooser::on_pushButton_5_clicked()
{
    this->close();
}

void strategychooser::loadStategies() {
    int i=0;
    QString value;

    QVector<QStringList> stratNames;
    QString qry;

    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");

    qry = "SELECT Name FROM Strategies";
    stratNames = db.select(qry);
    int size = stratNames.size();

    while (i < size) {
        value = stratNames.at(i).join("");
        ui->strategyList->addItem(new QListWidgetItem(value));

        i++;
    }
}

void strategychooser::clearTable() {
    while (ui->strategyList->count() > 0)
    {
        ui->strategyList->takeItem(0);
    }
}

void strategychooser::on_addBtn_clicked()
{
    this->close();
}

void strategychooser::on_pushButton_3_clicked()
{
    weakestLink = ui->strategyList->selectedItems().takeFirst()->text();
    //get sID
    QString stratName = weakestLink;
    QString qry = "SELECT strategyID FROM strategies WHERE name = '%1'";
    qry = qry.arg(stratName);
    QStringList stratID = db.selectCol(qry);

    //get CID
    qry = "SELECT constructID FROM constructs WHERE strategyID = '%1'";
    qry = qry.arg(stratID[0]);
    QStringList constructIDs = db.selectCol(qry);

    QString qry2;
    //delete relation
    //delete construct
    for (int i = 0; i < constructIDs.size(); i++) {
        qry  = "DELETE FROM relation WHERE cID = '%1'";
        qry2 = "DELETE FROM constructs WHERE constructID = '%1'";
        qry  = qry.arg(constructIDs[i]);
        qry2 = qry2.arg(constructIDs[i]);

        db.execSQL(qry);
        db.execSQL(qry2);

    }

    //delete strategy
    qry  = "DELETE FROM strategies WHERE strategyID = '%1'";
    qry  = qry.arg(stratID[0]);
    db.execSQL(qry);

    ui->strategyList->clear();
    loadStategies();
}
