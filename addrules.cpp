﻿/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "addrules.h"
#include "ui_addrules.h"

addrules::addrules(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addrules)
{

    ui->setupUi(this);

    connect(ui->saveButton, SIGNAL(clicked()),this, SLOT(onRuleSavedPressed()));

    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);
    this->setPalette(pal);
    this->setWindowTitle("Add Rules");
}

addrules::~addrules()
{
    delete ui;
}

void addrules::onRuleSavedPressed() {
    emit this->newRuleCreated();
}

void addrules::on_backButton_clicked()
{
    clearScreen();
    this->close();
}

void addrules::on_saveButton_clicked()
{
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
    QString qry = "INSERT INTO Rules(DeltaOperator, DeltaValue, ThetaOperator, ThetaValue, VolatilityOperator, VolatilityValue, ExpireValue, Action, Execution, GracePeriod,  ConstructID) VALUES ('%1','%2','%3','%4','%5','%6','%7','%8','%9','%10','%11')";

    qry = getRuleData(qry);

    db.execSQL(qry);
    clearScreen();
    this->close();

}

QString addrules::getRuleData(QString qry) {

    if (ui->deltaCheckBox->isChecked()) {
        qry = qry.arg(ui->deltaComboBox->currentText());
        qry = qry.arg(ui->deltaField->text());
    } else {
        qry = qry.arg('n');
        qry = qry.arg("null");
    }

    if (ui->thetaCheckBox->isChecked()) {
        qry = qry.arg(ui->thetaComboBox->currentText());
        qry = qry.arg(ui->thetaField->text());
    } else {
        qry = qry.arg('n');
        qry = qry.arg("null");
    }

    if (ui->volatilityCheckBox->isChecked()) {
        qry = qry.arg(ui->volatilityComboBox->currentText());
        qry = qry.arg(ui->volatilityField->text());
    } else {
        qry = qry.arg('n');
        qry = qry.arg("null");
    }

    if (ui->timeExpiryCheckBox->isChecked()) {
        qry = qry.arg(ui->timeExpiryField->text());
    } else {
        qry = qry.arg("null");
    }

    if (ui->buyBox->isChecked()) {
        qry = qry.arg("buy");
    } else {
        qry = qry.arg("sell");
    }

    if (ui->reinstateBox->isChecked()) {
        qry = qry.arg("reinstate");
    } else {
        qry = qry.arg("retain");
    }
    int grace = ui->graceEdit->text().toInt();
    qry = qry.arg(grace);

    return qry;
}

void addrules::clearScreen() {
    ui->buyBox->setChecked(false);
    ui->deltaCheckBox->setChecked(false);
    ui->deltaField->clear();
    ui->graceEdit->clear();
    ui->reinstateBox->setChecked(false);
    ui->retainBox->setChecked(false);
    ui->sellBox->setChecked(false);
    ui->thetaCheckBox->setChecked(false);
    ui->thetaField->clear();
    ui->timeExpiryCheckBox->setChecked(false);
    ui->timeExpiryField->clear();
    ui->volatilityCheckBox->setChecked(false);
    ui->volatilityField->clear();
}
