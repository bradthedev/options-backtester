/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "databaseloader.h"
#include "comboboxdelegate.h"
#include "benchtimer.h"
#include "ui_databaseloader.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QStandardItemModel>
#include <QtSql/QSql>
#include <QSqlRecord>
#include <QSqlField>
#include <QStringListModel>
#include <iostream>
#include <QDebug>
#include <QVector>
#include <QtMath>
#include "math.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

DatabaseLoader::DatabaseLoader(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DatabaseLoader),
    currentTable(Option)
{
    ui->setupUi(this);
    saving = false;
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);

    this->setPalette(pal);
    this->setWindowTitle("Database Loader");
}

void DatabaseLoader::openWindow()
{
    this->show();
    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
            //handle error and abort
        }
        //if(!db.isSetup()){ //checks if appropriate tables exist in DB

        if(!db.setup()){ //creates tables
            qDebug() << "db.setup() error:" << db.db.lastError();
            //handle error and abort
        }
    }
    updateSize();
}

DatabaseLoader::~DatabaseLoader()
{
    delete ui;
}

QStringList DatabaseLoader::selectFilesDialog()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(tr("Files (*.*)"));
    if (dialog.exec())
        return dialog.selectedFiles();
    else
        return QStringList();
}

void DatabaseLoader::on_btnLoadFile_clicked()
{
    //Get Files from dialog - TODO verify that all files are the same
    selectedFiles = selectFilesDialog();
    if(selectedFiles.size()<1){
        this->ui->filelist->setModel(new QStringListModel());
        return;
    }

    //Make listview show the selected file names
    ui->filelist->setModel(new QStringListModel(selectedFiles));
    //Use first file as a template to populate the tableview
    setupTableView(selectedFiles[0]);
    if (selectedFiles.length()>=0) {
        this->ui->btnSave->setEnabled(true);
    }
}

//////////////
/// Reads a CSV file into a QVector<QStringList> and returns it.
//////////////
QVector<QStringList> DatabaseLoader::readFile(QString filename)
{
    QVector<QStringList> fileContents;
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QTextStream in(&file);

    QString line= in.readLine();
    QStringList fields = line.split(",");
    //BenchTimer timer;

    //timer.display("readfile");

    if ((fields.size() != 19) && (this->currentTable == Option)){
      QMessageBox::information(0, "Needs Checking","Option file is not in the right format" );
      //DatabaseLoader::on_btnDelete_clicked();
      saving = false;
      //fileContents.clear();
    } else if ((fields.size() != 4) && (this->currentTable == Stock)){
        QMessageBox::information(0, "Needs Checking","Stock file is not in the right format" );
        //DatabaseLoader::on_btnDelete_clicked();
        saving = false;
        //fileContents.clear();
    } else if ((fields.size() != 8) && (this->currentTable == Interest)){
        QMessageBox::information(0, "Needs Checking","Interest file is not in the right format" );
        //DatabaseLoader::on_btnDelete_clicked();
        saving = false;
        //fileContents.clear();
    } else {
        fileContents.append(fields);
        while(!in.atEnd()){
            line = in.readLine();
            fields = line.split(",");
            fileContents.append(fields);
        }
        file.close();
    }
    return fileContents;
}

//////////////
/// Reads a CSV file and saves it to the database.
//////////////
bool DatabaseLoader::fileToDB(QString filename, DB_TABLE table)
{
    loading(QString("Loading %1").arg(filename));
    return db.loadData(correctData(readFile(filename),table),table);
}

//double DatabaseLoader::europeanOption(QStringList callPutData)
//{
//    /*double d1 = (Log(S / K) + (r - q + 0.5 * v ^ 2) * T) / (v * Sqr(T));
//    double d2 = (Log(S / K) + (r - q - 0.5 * v ^ 2) * T) / (v * Sqr(T));
//    double nd1 = Application.NormSDist(d1);
//    double nd2 = Application.NormSDist(d2);
//    double nnd1 = Application.NormSDist(-d1);
//    double nnd2 = Application.NormSDist(-d2);

//   if (CallOrPut = "Call")
//        return S * Exp(-q * T) * nd1 - K * Exp(-r * T) * nd2;
//    else
//        return -S * Exp(-q * T) * nnd1 + K * Exp(-r * T) * nnd2;*/

//    return 0.0;
//}

//double DatabaseLoader::impliedVolatility(QStringList callPutData)
//{
//    /*double dVol = 0.00001;
//    int maxIter = 100; // Max times allowed to guess the implied volatility
//    double vol_1 = 0.30; // Starting guessed implied volatility
//    double Value_1 = Value_2 = vol_2 = dx = 0.0;

//    for (int i = 0; i < maxIter; i++)
//    {
//        Value_1 = europeanOption(CallOrPut, S, K, vol_1, r, T, q);
//        vol_2 = vol_1 - dVol;
//        Value_2 = europeanOption(CallOrPut, S, K, vol_2, r, T, q);
//        dx = (Value_2 - Value_1) / dVol;
//        if (Abs(dx) < dVol) // Break out of the loop once the implied volatility is calculated
//        {
//            continue;
//        }
//        vol_1 = vol_1 - (OptionValue - Value_1) / dx;
//    }*/

//    return 0.0/*vol1*/;
//}

//void DatabaseLoader::calcGreeks(QStringList &callData, QStringList &putData)
//{
//    //double iVolatility = DatabaseLoader::impliedVolatility(callData); // The call and put have the same strike price and expiry date, so must have the same implied volatility

//    // callData[15] == Math.pow(e, (-q*t)) * nd1;
//    // putData[15] == Math.pow(e, (-q*t)) * (nd1 - 1);
//    // putData[16] == callData[16] == (Math.pow(e, (-q*t)) / (callData[1] * iVolatility * Math.sqrt(t)) * (1 / Math.sqrt(2 * Math.pi)) * (Math.pow(e, (Math.pow(-d1, 2) / 2))));
//    // callData[17] == ; // call theta code
//    // putData[17] == ; // put theta code
//    // putData[18] == callData[18] == ((1 / 100) * callData[1] * Math.pow(e, (-q*t)) * Math.sqrt(t)) * (1 / Math.sqrt(2 * Math.pi) * (Math.pow(e, (Math.pow(-d1, 2) / 2));
//}

QVector<QStringList> DatabaseLoader::correctData(QVector<QStringList> &filecontents, DB_TABLE table)
{
    const QString TEMP_DATE_FORMAT = "M/d/yyyy";
   //  qDebug() << filecontents.size();
    if((table == Option) && (filecontents.size() > 0)){
        for(int i = 1; i < filecontents.size(); i++)
        {
            if((filecontents[i][2]=="D")/* || (filecontents[i][10] == 0) || (filecontents[i][11] == 0)*/) // Extra checks for empty Bids and Asks, though not sure whether a Bid/Ask of 0 truly means invalid data
            {
                filecontents.remove(i); //D seems to stand for duplicate. fixes some annoying issues.
                i--;
            }
            /*if(i % 2 == 0) // For every call & put pair of data, recalculate the  financial "Greeks"
            {
                calcGreeks(filecontents[i], filecontents[i+1]);
            }*/
        }
    }
    if ((table == Stock) && (filecontents.size() > 0)) { //TO OPTIMIZE: copy filecontents to a linked list to speed up inserting.
        QDate prev = QDate::fromString(filecontents[1][0],TEMP_DATE_FORMAT).addDays(-1); //initialize to the day before the first day
        QDate current;
        for(int i = 1; i < filecontents.size(); i++){
            if(filecontents[i][0] == ""){
                filecontents.remove(i,1);
                i--;
                continue;
            }
            current = QDate::fromString(filecontents[i][0],TEMP_DATE_FORMAT);
            if(prev.addDays(1) != current){ //if there is a day missing
                int count;
                for(count = 1; prev.addDays(count) < current; count++); //count missing days
                double increment = (filecontents[i][1].toDouble() - filecontents[i-1][1].toDouble())/count; //amount to interpolate by
                for(int j = 1; j < count; j++){
                    QStringList qsl = filecontents[i-1];
                    qsl[0] = prev.addDays(j).toString(TEMP_DATE_FORMAT);
                    double temp = qsl[1].toDouble() + (double(j)*increment);
                    qsl[1] = QString::number(temp);
                    filecontents.insert(i-1+j,qsl);
                    //qDebug() << "inserted interpolated row";
                }
                i+=count-1;
            }
            prev = QDate::fromString(filecontents[i][0],TEMP_DATE_FORMAT);
        }

    }else if((table == Interest) && (filecontents.size() > 0)){

        ///Interpolate missing values (values that are currently -1)
        // **************************************************************************************//
        // AC 30/09/2014
     //   int count[9] = {0}; //count[0] = count rate3m missing since last, count[1] is for rate6m, etc.
    //    for(int i = 0; i < filecontents.size(); i++){
    //        for(int j = 0; j < 9; j++){
    //            if(filecontents[i][j+2]=="-1")
    //                count[j]++;
    //            else if(count[j]!=0){
    //                double diff = QString(filecontents[i-count[j]-1][j+2]).toDouble() - QString(filecontents[i][j+2]).toDouble();
    //                for(int n = 0; n < count[j]; n++){ //for each missing value
    //                    filecontents[i-count[j]+n][j+2] = QString::number(QString(filecontents[i-count[j]-1][j+2]).toDouble() - QString::number((diff/(count[j]+1))*(n+1)).toDouble());
    //                }
    //                count[j] = 0;
    //            }
    //        }
     //   }

    //    loading(QString("Converting to zero-coupon curve"));

        ///Create zero coupon curve
    //    double yield[10] = {0.0}; //yields for each row
    //    double product = 1.0;
    //    for(int i = 0; i < filecontents.size(); i++){
    //        for(int j = 0; j < 10; j++){
    //            yield[j] = 1.0+((100.0-filecontents[i][j+1].toDouble())*0.01)*(3.0/12.0);
    //            if(j==0)
    //                filecontents[i][j+1] = "0";
    //            else{
    //                product *= yield[j-1];
    //                filecontents[i][j+1] = QString::number( (12.0/(3.0*double(j)) )* qLn(product) );
    //            }
    //        }
     //       product = 1.0;
      //  }
   // }
   // return filecontents;
//}
         // AC 30/09/2014
        // **************************************************************************************//
        int count[9] = {0}; //count[0] = count rate3m missing since last, count[1] is for rate6m, etc.
        for(int i = 0; i < filecontents.size(); i++){
            for(int j = 0; j < 1; j++){
                if(filecontents[i][j+1]=="-1")
                    count[j]++;
                else if(count[j]!=0){
                    double diff = QString(filecontents[i-count[j]-1][j+1]).toDouble() - QString(filecontents[i][j+1]).toDouble();
                    for(int n = 0; n < count[j]; n++){ //for each missing value
                        filecontents[i-count[j]+n][j+1] = QString::number(QString(filecontents[i-count[j]-1][j+1]).toDouble() - QString::number((diff/(count[j]+1))*(n+1)).toDouble());
                    }
                    count[j] = 0;
                }
            }
        }

        loading(QString("Converting to zero-coupon curve"));

        ///Create zero coupon curve
        double yield[10] = {0.0}; //yields for each row
        double product = 1.0;
        for(int i = 0; i < filecontents.size(); i++){
            for(int j = 0; j < 1; j++){
                yield[j] = 1.0+((100.0-filecontents[i][j+1].toDouble())*0.01)*(3.0/12.0);
                if(j==0)
                    filecontents[i][j+1] = "0";
                else{
                    product *= yield[j-1];
                    filecontents[i][j+1] = QString::number( (12.0/(3.0*double(j)) )* qLn(product) );
                }
            }
            product = 1.0;
        }
    }
    return filecontents;
}
// **************************************************************************************//

QVector<QStringList> DatabaseLoader::mergeInterestData(QStringList files)
{
    loading(QString("Merging Files"));
    QVector<QStringList> merged;
    QStringList line;
    line.append("");
    line.append("");
    for(int i = 0; i < files.size(); i++){
        QVector<QStringList> temp = readFile(files[i]);
        int n = 0; //current line in merged vector
        for(int j = 0; j < temp.size();){ //j is current line in temp vector
            if(i==0){ //first file
                line[0] = temp[j][0];
                line[1] = temp[j][5];
                merged.append(line);
                j++;
            }else{
                if(QDate::fromString(merged[n][0],"M/d/yyyy") == QDate::fromString(temp[j][0],"M/d/yyyy") ){ //if the date matches
                    merged[n].append(temp[j][5]);
                    n++;
                    j++;
                }else if(QDate::fromString(merged[n][0],"M/d/yyyy") > QDate::fromString(temp[j][0],"M/d/yyyy")){
                    merged[n].append("-1");
                    n++;
                }else{ //this normally shouldn't happen unless the wrong data is inserted.
                    j++;
                }

            }
        }
    }
    return merged;
}

void DatabaseLoader::on_comboFileType_currentIndexChanged(int index)
{
    this->currentTable = DB_TABLE(index);
}

void DatabaseLoader::on_btnSave_clicked()
{   //bool saving = true;
     saving = true;

    loading(true);

    if ((selectedFiles.isEmpty()) || (selectedFiles[0] == "")){
        saving = false;
        QMessageBox::information(0, "Save Issue", "Please use the load button to select files to load into the database.");
    }
    //BenchTimer timer;
    if(this->currentTable == Interest && saving){//interest files need to be merged first
        db.loadData(correctData(mergeInterestData(selectedFiles),this->currentTable),this->currentTable);
    }else if (saving) {
        for(int i = 0; i < this->selectedFiles.size(); i++){
            fileToDB(this->selectedFiles[i],this->currentTable);
            //timer.display("Insert");
            //Update the listview as selected file names are processed
            selectedFiles[i] = "";
            ui->filelist->setModel(new QStringListModel(selectedFiles));
     //       qDebug() << "SqLite error:" << query.lastError().text() << ", SqLite error code:" << query.lastError().number();
        }
     }
    if (saving) {
    QMessageBox::information(0, "Finished", "Records were inserted into the database.");
    }
    loading(false);
    updateSize();
    this->ui->btnSave->setEnabled(false);
    // this->hide();
    // this->backtesterMain.openWindow(); // Moved the opening of the backtester main window to the continue button.
                                          // Allows for adding files multiple times and shows the change in size of the database when files are added.

}

void DatabaseLoader::on_btnContinue_clicked()
{
    this->close(); // The Backtester main window should go back to this screen, not the start
    this->backtesterMain.openWindow();
}

void DatabaseLoader::setupTableView(QString filename)
{
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", file.errorString());
        return;
    }

    //Get first line on csv file (headers)
    QTextStream in(&file);
    QString line = in.readLine();
    QStringList fields = line.split(",");

    QStandardItemModel* tableviewModel; //Datatype that can have a listview bound to it
    tableviewModel = new QStandardItemModel(2,8); //2 rows, 8 columns -TODO make dynamic

    //Show headers from CSV on tableview
    for(int i = 0; i < fields.size(); i++)
        tableviewModel->setHorizontalHeaderItem(i, new QStandardItem(fields[i]));
    //ui->tableView->setModel(tableviewModel); //bind listview to model

    //Show first data row of CSV on tableview
    line = in.readLine();
    fields = line.split(",");
    for(int i = 0; i<fields.size(); i++){
        QModelIndex index = tableviewModel->index(0, i, QModelIndex()); //get index
        tableviewModel->setData(index, QVariant(fields[i])); //insert at index
    }

    file.close();
}

//Could be updated to do something more fancy
void DatabaseLoader::loading(bool on)
{
    if(on){
        this->setEnabled(false);
    //  this->ui->lblStatus->setText("Loading");
    }
    else{
        this->setEnabled(true);
    //  this->ui->lblStatus->setText("");
    }
    qApp->processEvents();
}

void DatabaseLoader::loading(QryString status)
{
    // this->ui->lblStatus->setText(status);
    qApp->processEvents();
}

void DatabaseLoader::on_btnBack_clicked()
{
    this->close();
}

void DatabaseLoader::on_btnDelete_clicked()
{   // Deletes the data from the selected database table

    if (this->currentTable == Option){
       db.execSQL("DELETE FROM Option");
    }
    else if(this->currentTable == Stock){
       db.execSQL("DELETE FROM Stock");
    }
    else if(this->currentTable == Interest){
       db.execSQL("DELETE FROM Interest");
    }
    db.clear();
    updateSize();
    QMessageBox::information(0, "Finished", "The database has been deleted."); // Some notification for the user that the database has been deleted
}

void DatabaseLoader::updateSize()
{
    this->ui->txtSize->setText(QString::number(db.getDBSize(),'f',1)+" MB");

    if (this->ui->txtSize->text() == "0.0 MB") // Disallow continuing from the Database screen if the database is empty
        this->ui->btnContinue->setEnabled(false);
    else
        this->ui->btnContinue->setEnabled(true);
}
