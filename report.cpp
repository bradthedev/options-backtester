/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "report.h"
#include "DatabaseConstants.h"
#include <QDebug>

Report::Report():baseline(-1)
{
}

bool Report::load(QString filename, int reportno)
{
    if(this->data.size()-1 < reportno)
        data.resize(reportno+1);
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QTextStream in(&file);
    QStringList fields;
    QString line;
    RECORD r;

    while(!in.atEnd()){
        line = in.readLine();
        fields = line.split(",");
        if(fields.size()>=2){
            r.date = fields[0].toDouble();
            r.value = fields[1].toDouble();
            data[reportno].append(r);
        }
    }
    file.close();
    return true;
}

void Report::addRecord(QDate date, double capital, int reportno)
{

    if(this->data.size()-1 < reportno)
        data.resize(reportno+1);
    RECORD r;
    r.date = QDateTime(date).toTime_t();
    r.value = capital;
    this->data[reportno].append(r);
}

bool Report::saveAs(QString filename, int reportno)
{
    if(this->data.size()-1 < reportno)
        return false;

    QFile::remove(filename);

    QFile file(filename);
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);

    for(int i = 0; i < data[reportno].size(); i++){
        out << data[reportno][i].date << "," << data[reportno][i].value << "\n";
    }
    file.close();
    return true;
}

void Report::graph(QCustomPlot *plot)
{
    QString QtColours[]= { "red","black","green","blue","cyan","magenta","yellow", "gray", "darkRed", "darkGreen",
                           "darkBlue", "darkCyan", "darkMagenta", "darkYellow", "darkGray", "lightGray"};
    QColor Color;

    plot->clearGraphs();
    plot->clearItems();
    plot->legend->clear();
    double ymax = 0;
    double ymin = 0;
    for(int j = 0; j < this->data.size(); j++)
        for(int i = 0; i < this->data[j].size(); i++)
        {
            if(this->data[j][i].value<ymin)
                ymin = this->data[j][i].value;
            if(this->data[j][i].value>ymax)
                ymax = this->data[j][i].value;
        }

    // create graph and assign data to it:
    plot->legend->setVisible(true);
    QFont legendFont("Helvetica [Cronyx]", 12);
    legendFont.setPointSize(9);
    plot->legend->setFont(legendFont);
    plot->legend->setBrush(QBrush(QColor(255,255,255,230)));

    for(int i = 0; i < this->data.size(); i++){
        QVector<double> x,y;
        for(int j = 0; j < this->data[i].size(); j++){
            x.append(this->data[i][j].date);
            y.append(this->data[i][j].value);
        }
        plot->addGraph();

        Color.setNamedColor (QtColours[i]);
//        plot->graph(i)->setBrush(QBrush(Color));
        plot->graph(i)->setPen(QPen(Color));

//        if(i==0){
//            plot->graph(i)->setBrush(QBrush(QColor(0, 0, 255, 20)));
//            plot->graph(i)->setPen(QPen(QColor(0, 0, 255, 240)));
//        }
//        else if(i==1){
//            plot->graph(i)->setBrush(QBrush(QColor(255, 0, 0, 20)));
//            plot->graph(i)->setPen(QPen(QColor(255, 0, 0, 240)));
//        }
//        else if(i==2){
//            plot->graph(i)->setBrush(QBrush(QColor(0, 255, 0, 20)));
//            plot->graph(i)->setPen(QPen(QColor(0, 255, 0, 240)));
//        }
//        else{
//            plot->graph(i)->setBrush(QBrush(QColor(255, 255, 0, 20)));
//            plot->graph(i)->setPen(QPen(QColor(255, 255, 0, 240)));
//        }
        plot->graph(i)->setData(x, y);
        plot->graph(i)->setName(this->legendNames.at(i));
//        if(this->baseline == i){
//            this->legendNames.append(QString("Baseline"));
//            plot->graph(i)->setName(QString("Baseline"));
//        }
//        else{
//            this->legendNames.append(QString("Strategy %1").arg(i+1));
//            plot->graph(i)->setName(QString("Strategy %1").arg(i+1));
//        }
    }

    plot->xAxis->setLabel("Date");
    plot->yAxis->setLabel("Value");
    plot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    plot->xAxis->setDateTimeFormat("dd\nMMMM\nyyyy");
    plot->setInteraction(QCP::iRangeDrag, true);
    plot->setInteraction(QCP::iRangeZoom, true);

    plot->xAxis->setRange(this->data[0][0].date, this->data[0].last().date);
    plot->yAxis->setRange(ymin, ymax);
    plot->replot();
}

void Report::addLegend(QString name)
{
  this->legendNames.append(name);
}

void Report::setBaseline(int i)
{
    this->baseline = i;
}
