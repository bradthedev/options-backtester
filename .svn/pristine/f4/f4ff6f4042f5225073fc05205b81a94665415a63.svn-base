/*
 * Analysis Tool For Option Trading
 * Version 1.0
 *
 * Copyright 2013 Daniel Macdonald, Mit Naik, Joon Park.
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BACKTESTERMAINWINDOW_H
#define BACKTESTERMAINWINDOW_H

#include <QListWidgetItem>
#include <QWidget>
#include <QStringList>

#include "strategyeditorwindow.h"
#include "database.h"
#include "report.h"
#include "reportwindow.h"
#include "strategychooser.h"
#include "addrules.h"

namespace Ui {
class BacktesterMainWindow;
}

class BacktesterMainWindow : public QWidget
{
    Q_OBJECT
    
public:

    // BRAD CHECK OVER THIS
    QStringList expire;
    QStringList days;
    QStringList strike;
    QStringList action;
    QStringList type;
    QStringList strats;

    explicit BacktesterMainWindow(QWidget *parent = 0);
    void openWindow();
    void updateComboStrike();
    void updateComboExpire();
    void buyOptions(double &capital, OPTION &option); //Buys capital amount of market exposure in options
    double calcWorth(double money, QDate date, OPTION option);
    int diffMonths(QDate start, QDate end);
    strategychooser stratChooser;
    addrules rulesAdder;
    ~BacktesterMainWindow();

    void loadStrategy(QListWidgetItem *theStrategy);
    void setOffAskUnder(int index);

    StrategyEditorWindow stratEdit;
    QDate startDate, endDate;
    Report report;
    ReportWindow reportWindow;
    double strikePOffset; //percent offset
    Database db;

public slots:
    void onStrategySelected(const QList<QListWidgetItem *> &chosenStrategy);


private slots:
    void on_btnRun_clicked();

    //void on_comboExpire_currentIndexChanged(const QString &arg1);

    //void on_comboStrike_currentIndexChanged(const QString &arg1);

    void on_dateStart_userDateChanged(const QDate &date);

    void on_dateEnd_userDateChanged(const QDate &date);

    void on_btnQuit_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_3_clicked();

    void on_listWidget_currentTextChanged(const QString &currentText);

private:
    Ui::BacktesterMainWindow *ui;
};

#endif // BACKTESTERMAINWINDOW_H
