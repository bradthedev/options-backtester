/*
 * Analysis Tool For Option Trading
 * Version 1.0
 *
 * Copyright 2013 Daniel Macdonald, Mit Naik, Joon Park.
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "backtestermainwindow.h"
#include "ui_backtestermainwindow.h"
#include "database.h"
#include <QMessageBox>
#include <iostream>
#include <QTextStream>
#include <cmath>
#include "benchtimer.h"
#include <QDebug>
#include "report.h"

BacktesterMainWindow::BacktesterMainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BacktesterMainWindow)
{
    ui->setupUi(this);
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);

    QObject::connect(&stratChooser, SIGNAL(sendStrategy(QList<QListWidgetItem*>)),
                     this, SLOT(onStrategySelected(QList<QListWidgetItem*>)));

    this->setPalette(pal);
    this->setWindowTitle("Backtester Tool");
}

void BacktesterMainWindow::openWindow()
{
    this->show();
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
      this->startDate = db.selectDate("select min(date) from option");
      ui->dateStart->setDate(this->startDate);
      this->endDate = this->ui->dateEnd->date();
}

void BacktesterMainWindow::onStrategySelected(const QList<QListWidgetItem *> &chosenStrategy) {
    for (int i = 0; i < chosenStrategy.size(); i++) {
        ui->listWidget->addItem(chosenStrategy.front()->text());
    }
    ui->listWidget->repaint();
}

void BacktesterMainWindow::buyOptions(double &capital, OPTION &option)
{
    option.count = (int)capital/option.strike;
    capital = capital - (double(option.count)*option.ask);
}

int BacktesterMainWindow::diffMonths(QDate start, QDate end)
{
    int mDiff = end.month() - start.month();
    //int dDiff = end.toJulianDay() - start.toJulianDay();
    int yDiff = end.year() - start.year();
    //if(dDiff>365)
    mDiff += 12*yDiff;
    return mDiff;
}

BacktesterMainWindow::~BacktesterMainWindow()
{
    delete ui;
}

void BacktesterMainWindow::on_btnRun_clicked()
{
    //BenchTimer bench;
    //bench.qtimer.start();

    report.data.clear();
    double money = this->ui->spinStartMoney->value();
    this->setEnabled(false);
    qApp->processEvents();
    OPTION option;
    QVector<QStringList> optionValues;
    option.strike = strike.value(0).toDouble();
    option.ask = this->ui->txtAsk->text().toDouble();
    buyOptions(money,option);
    //not sure if needed option.expDate = QDate::fromString(this->ui->comboExpire->currentText(),DATE_FORMAT);
    QFile::remove("log.txt");
    QFile file("log.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream ss(&file);

    int months;
    if (days.value(0).toInt() != 0) {
        months = days.value(0).toInt()/30;
    } else {
        months = diffMonths(this->startDate,QDate::fromString(expire.value(0),DATE_FORMAT));
    }
    ss << "Start date: " << this->startDate.toString(DATE_FORMAT) << "\n"
       << "End date: " << this->endDate.toString(DATE_FORMAT) << "\n"
       << "Starting Money: $" << this->ui->spinStartMoney->value() << "\n\n";
    money -= this->ui->txtAsk->text().toDouble() * option.count;
    ss << "Starting Purchase:\n"
             << " Options: " << option.count << "\n"
             << " Strike:  " << option.strike << "\n"
             << " Expire:  " << option.expDate.toString(DATE_FORMAT) << "\n"
             //Edited for GUI << " Ask:     " << this->ui->txtAsk->text().toDouble() << "\n"
             << "Money: $" << money << "\n\n";

    QryString qry = "select bid from Option where date = '%1' and ExpireDate = '%2' and strike = %3 and type = 'call'";
    qry = qry.arg(this->startDate);
    qry = qry.arg(option.expDate);
    qry = qry.arg(option.strike);
    double bid = db.selectSingle(qry).toDouble();
    this->report.addRecord(this->startDate,money+(option.count * ((this->ui->txtAsk->text().toDouble()+bid)/2)),0);

    //grab all of the option pricing for above strike and expiry
    if(this->ui->boolDaily->isChecked()){
        qry = "select date, bid, ask from option where ExpireDate = '%1' and strike = %2 and type = 'call'";
        qry = qry.arg(option.expDate);
        qry = qry.arg(option.strike);
        optionValues = db.select(qry);
    }

    for(QDate date = this->startDate; date < this->endDate; date = date.addDays(1)){
        if(date>=option.expDate){
        //after or on expiry
            if(option.count != 0){
            //if options haven't been excersized yet
                QryString qstr = "select Price from Stock where Date = '%1'";
                qstr = qstr.arg(date);
                QString currentPrice = db.selectSingle(qstr);
                if(currentPrice.toDouble()>option.strike){
                    ss << date.toString(DATE_FORMAT) << "\n"
                             << option.count << " options were excercised in the money.\n"
                             << " Strike: $" << option.strike << "\n"
                             << " Current price: $" << currentPrice.toDouble() << "\n";
                    money += (currentPrice.toDouble()-option.strike)*option.count;
                }else{
                    ss << date.toString(DATE_FORMAT) << "\n"
                             << option.count << " options expired out of the money.\n"
                             << " Strike: $" << option.strike << "\n"
                             << " Current price: $" << currentPrice.toDouble() << "\n";
                }
                ss << "Money: $" << money << "\n\n";
                this->report.addRecord(date,money,0);

                option.count = 0;
            }

            if(db.count(QryString("select count(*) from option where date = '%1' and type='call'").arg(date))>0){
            //if there are available options for this date
                option = db.selectClosestOption(date,months,this->strikePOffset,"call");
                if(option.expDate > this->endDate)
                    break;
                buyOptions(money,option);
                ss << date.toString(DATE_FORMAT) << "\n"
                         << "Purchased:   " << option.count << " options. " << "\n"
                         << " Strike:     " << option.strike << "\n"
                         << " Underlying: " << option.underlying << "\n"
                         << " Expiring:   " << option.expDate.toString(DATE_FORMAT) << "\n"
                         << " Ask:        " << option.ask << "\n"
                         << "Money: $" << money << "\n\n";

                ///Grab all of the option pricing for above strike and expiry
                ///This saves a ton of disc reads since it's only done once every expiry instead of every day.
                if(this->ui->boolDaily->isChecked()){
                    QryString qry = "select date, bid, ask from option where ExpireDate = '%1' and strike = %2 and type = 'call'";
                    qry = qry.arg(option.expDate);
                    qry = qry.arg(option.strike);
                    optionValues = db.select(qry);
                }
            }
        }else{ //non-action day
            if(date == this->startDate)
                continue; //this day is already logged.
            if(this->ui->boolDaily->isChecked()){
                if(option.count==0)
                    ss << "error?";

                ///Find the correct option price from optionValues
                int index = -1;
                QString sdate = date.toString(DATE_FORMAT);
                for(int i = 0; i < optionValues.size(); i++)
                    if(optionValues[i][0]==sdate)
                        if(index == -1 || optionValues[i][1]>optionValues[index][1]) //grab the one with the biggest bid if multiple on same date
                            index = i;
                if(index == -1){//no dates matched, skip logging this day
                    qDebug() << date;
                    continue;
                }
                if(optionValues[index][1].toDouble()+optionValues[index][2].toDouble()==0 && option.expDate.addDays(-1) == QDate::fromString(optionValues[index][0],DATE_FORMAT))
                    continue; //occasionally, on the last day before expiry, the bid and ask prices are 0. These create wierd spikes so they are skipped.

                ///Add worth to the graph
                double price = (optionValues[index][1].toDouble() + optionValues[index][2].toDouble())/2;
                this->report.addRecord(date,money+(option.count * price),0);

                if(this->ui->boolLogDaily->isChecked())
                    ss << "\nDay:" << date.toString(DATE_FORMAT) << " Total worth:" << money+(option.count * price) << " Bid:" << optionValues[index][1] << " Ask:" << optionValues[index][2] << "\n";

//                if(optionValues[index][1].toDouble()+optionValues[index][2].toDouble()!=0){
//                    worth += option.count * price;
//                    this->report.addRecord(date,worth,0);
//                }

            }
        }
    }
    ss << "end backtest\n";

    file.close();

    qry = "select date, price from stock where date > '%1' and date < '%2' order by date asc";
    qry = qry.arg(this->startDate).arg(this->endDate);
    QVector<QStringList> baseline = db.select(qry);
    double stockQuant = this->ui->spinStartMoney->value()/baseline[0][1].toDouble();
    for (int i = 0; i < baseline.size(); i++)
        this->report.addRecord(QDate::fromString(baseline[i][0],DATE_FORMAT),baseline[i][1].toDouble()*stockQuant,1);
    this->report.setBaseline(1);
    this->report.saveAs("TestReport.csv",0);
    this->reportWindow.show();
    this->reportWindow.reset();
    this->reportWindow.drawGraph(report);
    this->setEnabled(true);
    qApp->processEvents();
    //bench.display("backtest finished: ");
}

void BacktesterMainWindow:: setOffAskUnder() {
    updateComboStrike();
    int months;
    if (days.value(0).toInt() != 0) {
        months = days.value(0).toInt()/30;
    } else {
        months = diffMonths(this->startDate,QDate::fromString(expire.value(0),DATE_FORMAT));
    }
    QryString qryStr = "select OptionID from Option where date = '%1' and ExpireDate = '%2' and type = 'call' and strike = %3";
    qryStr = qryStr.arg(this->startDate).arg(QDate::fromString(expire.value(0),DATE_FORMAT)).arg(strike.value(0).toDouble());
    int ID = db.selectSingle(qryStr).toInt();
    OPTION option = db.selectOption(ID);
    //QString lastpriceStr = db.selectSingle(QryString("select distinct(lastprice) from option where date = '%1'").arg(this->startDate));
    double offset = option.strike - option.underlying;
    double percentOffset = 1-(option.underlying/option.strike);
    this->ui->txtOffset->setText(QString::number(offset)+ " (" + QString::number(percentOffset*100,'f',1) + "%)");
    this->ui->txtPrice->setText(QString::number(option.underlying));
    this->ui->txtAsk->setText(QString::number(option.ask));
    this->strikePOffset = percentOffset;

}

void BacktesterMainWindow::on_dateStart_userDateChanged(const QDate &date)
{
    this->startDate = date;
    updateComboExpire();
    updateComboStrike();
}

void BacktesterMainWindow::on_dateEnd_userDateChanged(const QDate &date)
{
    this->endDate = date;
}

void BacktesterMainWindow::on_btnQuit_clicked()
{
    this->hide();
}

void BacktesterMainWindow::on_pushButton_2_clicked()
{
    this->stratChooser.openWindow();
}

void BacktesterMainWindow::on_pushButton_5_clicked()
{
    this->rulesAdder.show();
}

void BacktesterMainWindow::on_pushButton_3_clicked()
{
    if (ui->listWidget->selectedItems().size() > 0){
        ui->listWidget->takeItem(ui->listWidget->row(ui->listWidget->selectedItems().takeFirst()));
    }
}

void BacktesterMainWindow::on_listWidget_currentTextChanged(const QString &currentText)
{
    qDebug() << "Here";
    //Open file that contains the strategy and read it in
    QFile f("strategies.csv");
    QString data;
    QStringList vals;
    if (f.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QTextStream in(&f);
          while ( !in.atEnd() ) {
             QString line = in.readLine();
             ;
             vals.append(line.split(","));
          }
      }

     f.close();

     int i = 0;
     while ( i < vals.size()) {
         if (vals.value(i) == currentText) {
             expire.append(vals.value(++i));
             days.append(vals.value(++i));
             strike.append(vals.value(++i));
             action.append(vals.value(++i));
             type.append(vals.value(++i));
         } else {
             i++;
         }
     }
    setOffAskUnder();
    qDebug() << vals.value(vals.indexOf(currentText)) << " the End";
}



//Old possibly not needed methods

void BacktesterMainWindow::updateComboStrike()
{
    QryString qryStr;
    qryStr = "select strike from Option where date = '%1' and expiredate = '%2' and type='call'";

    //Edited for GUI qryStr = qryStr.arg( ui->dateStart->date() ).arg( ui->comboExpire->currentText() );
    //qryStr.operator =()
    //Edited for GUI ui->comboStrike->clear();
    //Edited for GUI ui->comboStrike->insertItems(0,db.selectCol(qryStr));
}

void BacktesterMainWindow::updateComboExpire()
{
    QryString qryStr;
    qryStr = "select distinct(ExpireDate) from Option where date = '%1' and type='call'";
    qryStr = qryStr.arg(ui->dateStart->date());
    //Edited for GUI ui->comboExpire->clear();
    //Edited for GUI ui->comboExpire->insertItems(0,db.selectCol(qryStr));
}


