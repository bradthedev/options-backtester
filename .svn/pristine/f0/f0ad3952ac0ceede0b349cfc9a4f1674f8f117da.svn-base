/*
 * Analysis Tool For Option Trading
 * Version 1.0
 *
 * Copyright 2013 Daniel Macdonald, Mit Naik, Joon Park.
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "backtestermainwindow.h"
#include "ui_backtestermainwindow.h"
#include "database.h"
#include <QMessageBox>
#include <iostream>
#include <QTextStream>
#include <cmath>
#include "benchtimer.h"
#include <QDebug>
#include "report.h"

BacktesterMainWindow::BacktesterMainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BacktesterMainWindow)
{
    ui->setupUi(this);
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);

    QObject::connect(&stratChooser, SIGNAL(sendStrategy(QList<QListWidgetItem*>)),
                     this, SLOT(onStrategySelected(QList<QListWidgetItem*>)));

    QObject::connect(&rulesAdder, SIGNAL(sendRule(QString)),
                     this, SLOT(onRuleSelected(QString)));


    this->setPalette(pal);
    this->setWindowTitle("Backtester Tool");
}

void BacktesterMainWindow::openWindow()
{
    this->show();
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
      this->startDate = db.selectDate("select min(date) from option");
      ui->dateStart->setDate(this->startDate);
      this->endDate = this->ui->dateEnd->date();
}

void BacktesterMainWindow::onStrategySelected(const QList<QListWidgetItem *> &chosenStrategy) {
    for (int i = 0; i < chosenStrategy.size(); i++) {
        ui->listWidget->addItem(chosenStrategy.front()->text());
        loadStrategy(chosenStrategy.front());
    }
    ui->listWidget->repaint();
}

void BacktesterMainWindow::onRuleSelected(QString rule) {

        //Get ConstructID
        int cid = getCID();
        //Get RuleID
        int rid = getRID(rule);//of all bugs
        //Add RuleID and ConstructID to relation
        QString qry = "INSERT INTO Relation(cID,rID) VALUES ('%1','%2')";
        qry = qry.arg(cid);
        qry = qry.arg(rid);

        db.execSQL(qry);
        //Print all relations that have the same CID as above

        //loadSelectedRule(rule);
        ui->listWidget_2->repaint();
}

int BacktesterMainWindow::getCID() {

    QString cur = ui->constructList->currentItem()->text();
    QStringList curAtt = cur.split(" ");

    QString date;

    if (curAtt[0] != 0) {
        for (int i = 0; i < 4; i++) {
            date += curAtt[0] + " ";
            curAtt.removeFirst();
        }
        date = date.trimmed();
    } else {
        date == 0;
    }

    QString findConsQry = "SELECT ConstructID FROM Constructs WHERE ExpireDate = '%1' AND ExpireDays = '%2' AND StrikePrice = '%3' AND Type = '%4' AND Action = '%5'";
    findConsQry = findConsQry.arg(date);

    for (int i = 0; i < curAtt.size()-1; i++) {
        findConsQry = findConsQry.arg(curAtt[i]);
    }

    QVector<QStringList> CID = db.select(findConsQry);

    return CID[0].at(0).toInt();
}

int BacktesterMainWindow::getRID(QString rule) {

    int ridVec(ui->listWidget_2->count());

    QString qry = "SELECT * FROM Rules";
    QVector<QStringList> listOfRules = db.select(qry);

    QString aRule;

    for (int i = 0; i < listOfRules.size(); i++) {
        aRule = "";
        for (int j = 0; j < listOfRules[i].size()-1; j++) {
            aRule += listOfRules[i].at(j) + " ";
        }
            if (aRule == rule) {
                return (listOfRules[i].at(0).toInt());
            }
    }

    return -1;
}

//void BacktesterMainWindow::loadSelectedRule(QString rule) {
//    ui->listWidget_2->clear();

//    if(!db.db.isOpen()){
//        if(!db.openDB()){ //opens or creates the database
//            qDebug() << "db.openDB error:" << db.db.lastError();
//        }
//    }

//     QString getConsQry = "SELECT Strategies.StrategyID, Constructs.ConstructID FROM Constructs, Strategies WHERE Strategies.StrategyID = Constructs.StrategyID AND Strategies.Name = '%1'";
//     getConsQry = getConsQry.arg(ui->listWidget->selectedItems().at(0)->text());

//     QVector<QStringList> constructs = db.select(getConsQry);
//     QString ruleString;
//     QString qry;
//     QVector<QStringList> rules;

//        qry = "SELECT * FROM Rules WHERE Rules.ConstructID = '%1'";

//        //started of random but isnt anymore. it pics the construct foreign key value that relates to the selected foreign key
//        int randomfuckingnumber = (constructs[ui->constructList->selectedItems().at(0)->text().toInt()].at(0)).toInt();
//        qry = qry.arg(randomfuckingnumber - 1); // minus one cause auto index dont start at 0
//        rules = db.select(qry);

//        for (int i = 0; i < rules.size(); i++) {
//            ruleString = "";

//            for (int j = 0; j < rules[i].size(); j++) {
//                ruleString += rules[i].at(j) + " ";
//            }

//            ui->listWidget_2->addItem(ruleString);
//        }
//}

void BacktesterMainWindow::loadStrategy(QListWidgetItem *theStrategy) {
    //Open file that contains the strategy and read it in
//    QFile f("strategies.csv");
//    QString data;
//    QStringList vals;
//    if (f.open(QIODevice::ReadOnly | QIODevice::Text))
//    {

//        QTextStream in(&f);
//          while ( !in.atEnd() ) {
//             QString line = in.readLine();
//             ;
//             vals.append(line.split(","));
//          }
//      }

//     f.close();

    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
        }
    }

     QString stratName = theStrategy->text();

     QString qry = "SELECT * FROM Constructs, Strategies WHERE Strategies.StrategyID = Constructs.StrategyID AND Strategies.Name = '%1'";
     qry = qry.arg(stratName);

     QVector<QStringList> vals;
     vals = db.select(qry);

     int i = 0;
     strats.append(stratName);

     QStringList expire;
     QStringList days;
     QStringList strike;
     QStringList action;
     QStringList type;

     //QString constuctString;

     while ( i < vals.size()) {
             expire.append(vals.at(i).value(1));
             days.append(vals.at(i).value(2));
             strike.append(vals.at(i).value(3));
             action.append(vals.at(i).value(4));
             type.append(vals.at(i).value(5));
             i++;

     }
     expireList.append(expire);
     daysList.append(days);
     strikeList.append(strike);
     actionList.append(action);
     typeList.append(type);

}

void BacktesterMainWindow::buyOptions(double &capital, OPTION &option)
{
    option.count = (int)capital/option.strike;
    capital = capital - (double(option.count)*option.ask);
}

int BacktesterMainWindow::diffMonths(QDate start, QDate end)
{
    int mDiff = end.month() - start.month();
    //int dDiff = end.toJulianDay() - start.toJulianDay();
    int yDiff = end.year() - start.year();
    //if(dDiff>365)
    mDiff += 12*yDiff;
    return mDiff;
}

BacktesterMainWindow::~BacktesterMainWindow()
{
    delete ui;
}

void BacktesterMainWindow::on_btnRun_clicked()
{
    //BenchTimer bench;
    //bench.qtimer.start();
    int stratCount = 0;
    report.data.clear();
    report.legendNames.clear();
    QFile::remove("log.txt");
    QFile file("log.txt");

    QTextStream ss(&file);

    double startingCash = this->ui->spinStartMoney->value();
  for (int x = 0; x < ui->listWidget->count(); x++) { // replace 1 with ui->listWidget->count()
      file.open(QIODevice::WriteOnly | QIODevice::Text);
    for (int y = 0; y < 1; y++) { //replace 1 with expireList[x].size()
        setOffAskUnder(y, x);

        double money = startingCash;//this->ui->spinStartMoney->value();
        this->setEnabled(false);
        qApp->processEvents();
        OPTION option;
        QVector<QStringList> optionValues;
        option.strike = strikeList[x].value(y).toDouble();
        option.ask = this->ui->txtAsk->text().toDouble();
        buyOptions(money,option);
        //not sure if needed option.expDate = QDate::fromString(this->ui->comboExpire->currentText(),DATE_FORMAT);


        int months;
        if (daysList[x].value(0).toInt() != 0) {
            months = daysList[x].value(y).toInt()/30;
        } else {
            months = diffMonths(this->startDate,QDate::fromString(expireList[x].value(y),DATE_FORMAT));
        }
        ss << "Start date: " << this->startDate.toString(DATE_FORMAT) << "\n"
           << "End date: " << this->endDate.toString(DATE_FORMAT) << "\n"
           << "Starting Money: $" << this->ui->spinStartMoney->value() << "\n\n";
        money -= this->ui->txtAsk->text().toDouble() * option.count;
        ss << "Starting Purchase:\n"
                 << " Options: " << option.count << "\n"
                 << " Strike:  " << option.strike << "\n"
                 << " Expire:  " << option.expDate.toString(DATE_FORMAT) << "\n"
                 //Edited for GUI << " Ask:     " << this->ui->txtAsk->text().toDouble() << "\n"
                 << "Money: $" << money << "\n\n";

        QryString qry = "select bid from Option where date = '%1' and ExpireDate = '%2' and strike = %3 and type = '%4'";
        qry = qry.arg(this->startDate);
        qry = qry.arg(option.expDate);
        qry = qry.arg(option.strike);
        qry = qry.arg(typeList[x].at(y));
        double bid = db.selectSingle(qry).toDouble();
        this->report.addRecord(this->startDate,money+(option.count * ((this->ui->txtAsk->text().toDouble()+bid)/2)),x);

        //grab all of the option pricing for above strike and expiry
        if(this->ui->boolDaily->isChecked()){
            qry = "select date, bid, ask from option where ExpireDate = '%1' and strike = %2 and type = '%3'";
            qry = qry.arg(option.expDate);
            qry = qry.arg(option.strike);
            qry = qry.arg(typeList[x].at(y));
            optionValues = db.select(qry);
        }

        for(QDate date = this->startDate; date < this->endDate; date = date.addDays(1)){
            if(date>=option.expDate){
            //after or on expiry
                if(option.count != 0){
                //if options haven't been excersized yet
                    QryString qstr = "select Price from Stock where Date = '%1'";
                    qstr = qstr.arg(date);
                    QString currentPrice = db.selectSingle(qstr);
                    if(currentPrice.toDouble()>option.strike){
                        ss << date.toString(DATE_FORMAT) << "\n"
                                 << option.count << " options were excercised in the money.\n"
                                 << " Strike: $" << option.strike << "\n"
                                 << " Current price: $" << currentPrice.toDouble() << "\n";
                        money += (currentPrice.toDouble()-option.strike)*option.count;
                    }else{
                        ss << date.toString(DATE_FORMAT) << "\n"
                                 << option.count << " options expired out of the money.\n"
                                 << " Strike: $" << option.strike << "\n"
                                 << " Current price: $" << currentPrice.toDouble() << "\n";
                    }
                    ss << "Money: $" << money << "\n\n";
                    this->report.addRecord(date,money,x);

                    option.count = 0;
                }

                if(db.count(QryString("select count(*) from option where date = '%1' and type='%2'").arg(date).arg(typeList[x].at(y)))>0){
                //if there are available options for this date
                    option = db.selectClosestOption(date,months,this->strikePOffset,typeList[x].at(y));
                    if(option.expDate > this->endDate)
                        break;
                    buyOptions(money,option);
                    ss << date.toString(DATE_FORMAT) << "\n"
                             << "Purchased:   " << option.count << " options. " << "\n"
                             << " Strike:     " << option.strike << "\n"
                             << " Underlying: " << option.underlying << "\n"
                             << " Expiring:   " << option.expDate.toString(DATE_FORMAT) << "\n"
                             << " Ask:        " << option.ask << "\n"
                             << "Money: $" << money << "\n\n";

                    ///Grab all of the option pricing for above strike and expiry
                    ///This saves a ton of disc reads since it's only done once every expiry instead of every day.
                    if(this->ui->boolDaily->isChecked()){
                        QryString qry = "select date, bid, ask from option where ExpireDate = '%1' and strike = %2 and type = '%3'";
                        qry = qry.arg(option.expDate);
                        qry = qry.arg(option.strike);
                        qry = qry.arg(typeList[x].at(y));
                        optionValues = db.select(qry);
                    }
                }
            }else{ //non-action day
                if(date == this->startDate)
                    continue; //this day is already logged.
                if(this->ui->boolDaily->isChecked()){
                    if(option.count==0)
                        ss << "error?";

                    ///Find the correct option price from optionValues
                    int index = -1;
                    QString sdate = date.toString(DATE_FORMAT);
                    for(int i = 0; i < optionValues.size(); i++)
                        if(optionValues[i][0]==sdate)
                            if(index == -1 || optionValues[i][1]>optionValues[index][1]) //grab the one with the biggest bid if multiple on same date
                                index = i;
                    if(index == -1){//no dates matched, skip logging this day
                        qDebug() << date;
                        continue;
                    }
                    if(optionValues[index][1].toDouble()+optionValues[index][2].toDouble()==0 && option.expDate.addDays(-1) == QDate::fromString(optionValues[index][0],DATE_FORMAT))
                        continue; //occasionally, on the last day before expiry, the bid and ask prices are 0. These create wierd spikes so they are skipped.

                    ///Add worth to the graph
                    double price = (optionValues[index][1].toDouble() + optionValues[index][2].toDouble())/2;
                    this->report.addRecord(date,money+(option.count * price),x);

                    if(this->ui->boolLogDaily->isChecked())
                        ss << "\nDay:" << date.toString(DATE_FORMAT) << " Total worth:" << money+(option.count * price) << " Bid:" << optionValues[index][1] << " Ask:" << optionValues[index][2] << "\n";

    //                if(optionValues[index][1].toDouble()+optionValues[index][2].toDouble()!=0){
    //                    worth += option.count * price;
    //                    this->report.addRecord(date,worth,x);
    //                }

                }
            }
        }

    }
    ss << "end backtest\n";

    this->report.addLegend(this->strats.at(x));
    stratCount = x;

    file.close();
  }
    QryString stockQry;
    stockQry = "select date, price from stock where date > '%1' and date < '%2' order by date asc";
    stockQry = stockQry.arg(this->startDate).arg(this->endDate);


    //Must Have Stock Data Entered Checker
    QVector<QStringList> baseline = db.select(stockQry);
    if (baseline.size() < 1) {
        QMessageBox stockBox;
        stockBox.setText("You must enter Stock Data before continuing");
        stockBox.exec();
        this->close();
        return;
    } else {
    double stockQuant = this->ui->spinStartMoney->value()/baseline[0][1].toDouble();

    for (int i = 0; i < baseline.size(); i++)
        this->report.addRecord(QDate::fromString(baseline[i][0],DATE_FORMAT),baseline[i][1].toDouble()*stockQuant,stratCount+1);
    this->report.setBaseline(1);
    this->report.addLegend("Baseline");
    this->report.saveAs("TestReport.csv",0);
    this->reportWindow.show();
    this->reportWindow.reset();
    this->reportWindow.drawGraph(report);
    this->setEnabled(true);
    qApp->processEvents();
    //bench.display("backtest finished: ");
    }
}

void BacktesterMainWindow::setOffAskUnder(int index, int stratIndex) {

    QDate validExpire;
    QDate expireAsDate = QDate::fromString(expireList[stratIndex].value(index));
//    int months;
    if (daysList[stratIndex].value(index).toInt() != 0) {
        validExpire = calculateValidExpiry((ui->dateStart->date().addDays(daysList[stratIndex].value(index).toInt()).toString()), strikeList[stratIndex].value(index), index, stratIndex);
//        months = days.value(index).toInt()/30;
    } else {
        validExpire = calculateValidExpiry(expireAsDate.toString(), strikeList[stratIndex].value(index), index, stratIndex);
//        months = diffMonths(this->startDate,QDate::fromString(expire.value(index)));
    }



    QryString qryStr = "select OptionID from Option where date = '%1' and ExpireDate = '%2' and type = '%3' and strike = %4";
    qryStr = qryStr.arg(this->startDate).arg(validExpire).arg(typeList[stratIndex].at(index)).arg(strikeList[stratIndex].value(index).toDouble());
    int ID = db.selectSingle(qryStr).toInt();
    OPTION option = db.selectOption(ID);
    qDebug() << " ->value of underlying price " << option.underlying << "   -> the stike value: " << option.strike;
    //QString lastpriceStr = db.selectSingle(QryString("select distinct(lastprice) from option where date = '%1'").arg(this->startDate));
    double offset = option.strike - option.underlying;
    double percentOffset = 1-(option.underlying/option.strike);
    this->ui->txtOffset->setText(QString::number(offset)+ " (" + QString::number(percentOffset*100,'f',1) + "%)");
    this->ui->txtPrice->setText(QString::number(option.underlying));
    this->ui->txtAsk->setText(QString::number(option.ask));
    this->strikePOffset = percentOffset;

}

QDate BacktesterMainWindow::calculateValidExpiry(QString expire, QString strike, int currentIndex, int stratIndex) {
    QryString qryStr;
    QStringList results;
    int diff;
    int index;
    qryStr = "select distinct(ExpireDate) from Option where date = '%1' and type='%2' and strike='%3'";
    qryStr = qryStr.arg(ui->dateStart->date()).arg(typeList[stratIndex].at(currentIndex)).arg(strike);
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
    results = db.selectCol(qryStr);

    QString temp;

    for (int i = 0; i < results.size(); i++) {
        temp = results.takeFirst();
        temp.remove('-');
        results.push_back(temp);
    }

    QDate resDate = QDate::fromString(results.at(0), "yyyyMMdd");
    QDate expireDate = QDate::fromString(expire);

    diff = resDate.daysTo(expireDate);
    index = 0;
    for (int i = 1; i < results.size(); i++) {
        resDate = QDate::fromString(results.at(i), "yyyyMMdd");

        if (resDate.daysTo(expireDate) < diff) {
            diff = resDate.daysTo(expireDate);
            index = i;
        }
    }

    QDate resultDate = QDate::fromString(results.at(index), "yyyyMMdd");
    resultDate.setDate(resultDate.year(),resultDate.month(),resultDate.day());
    return resultDate;
}

void BacktesterMainWindow::on_dateStart_userDateChanged(const QDate &date)
{
    this->startDate = date;
    updateComboExpire();
    updateComboStrike();
}

void BacktesterMainWindow::on_dateEnd_userDateChanged(const QDate &date)
{
    this->endDate = date;
}

void BacktesterMainWindow::on_btnQuit_clicked()
{
    this->close();
}

void BacktesterMainWindow::on_pushButton_2_clicked()
{
    this->stratChooser.openWindow();
}

void BacktesterMainWindow::on_pushButton_5_clicked()
{
    this->rulesAdder.open();
}

void BacktesterMainWindow::on_pushButton_3_clicked()
{
    if (ui->listWidget->selectedItems().size() > 0){
        ui->listWidget->takeItem(ui->listWidget->row(ui->listWidget->selectedItems().takeFirst()));
    }
}

void BacktesterMainWindow::on_listWidget_currentTextChanged(const QString &currentText)
{
     ui->constructList->clear();
     QString stratName = currentText;

     int index = strats.indexOf(stratName);

     QString constuctString;
     int i = 0;
     while ( i < expireList[index].size()) {
             constuctString = "";

             constuctString += expireList[index].at(i) + " ";

             constuctString += daysList[index].at(i) + " ";

             constuctString += strikeList[index].at(i) + " ";

             constuctString += actionList[index].at(i) + " ";

             constuctString += typeList[index].at(i) + " ";

             ui->constructList->addItem(constuctString);
             i++;

     }
    //setOffAskUnder(strats.indexOf(currentText));
}



//Old possibly not needed methods

void BacktesterMainWindow::updateComboStrike()
{
    QryString qryStr;
    qryStr = "select strike from Option where date = '%1' and expiredate = '%2' and type='call'";

    //Edited for GUI qryStr = qryStr.arg( ui->dateStart->date() ).arg( ui->comboExpire->currentText() );
    //qryStr.operator =()
    //Edited for GUI ui->comboStrike->clear();
    //Edited for GUI ui->comboStrike->insertItems(0,db.selectCol(qryStr));
}

void BacktesterMainWindow::updateComboExpire()
{
    QryString qryStr;
    qryStr = "select distinct(ExpireDate) from Option where date = '%1' and type='call'";
    qryStr = qryStr.arg(ui->dateStart->date());
    //Edited for GUI ui->comboExpire->clear();
    //Edited for GUI ui->comboExpire->insertItems(0,db.selectCol(qryStr));
}

void BacktesterMainWindow::on_constructList_currentTextChanged(const QString &currentText)
{
    ui->pushButton_5->setEnabled(true);
    setOffAskUnder(ui->constructList->currentIndex().row(), ui->listWidget->currentIndex().row());
}
