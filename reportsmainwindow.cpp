/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reportsmainwindow.h"
#include "ui_reportsmainwindow.h"
#include <QFileDialog>
#include <QStringListModel>

ReportsMainWindow::ReportsMainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReportsMainWindow)
{
    ui->setupUi(this);
}

ReportsMainWindow::~ReportsMainWindow()
{
    delete ui;
}

QStringList ReportsMainWindow::selectFilesDialog()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setNameFilter(tr("Files (*.*)"));
    if (dialog.exec())
        return dialog.selectedFiles();
    else
        return QStringList();
}

void ReportsMainWindow::on_btnStratLoad_clicked()
{
    this->report.data.clear();
    this->selectedFiles = selectFilesDialog();
    if(this->selectedFiles.size()<1){
        return;
    }

    //Make listview show the selected file names
    ui->filelist->setModel(new QStringListModel(selectedFiles));


    for(int i = 0; i < this->selectedFiles.size(); i++){
        this->report.load(this->selectedFiles[i],i);
    }

    this->reportWindow.reset();
    this->reportWindow.drawGraph(report);

    this->reportWindow.show();

}

void ReportsMainWindow::on_btnBack_clicked()
{
    this->close();
}
