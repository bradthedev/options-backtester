/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ruleschooser.h"
#include "ui_ruleschooser.h"

rulesChooser::rulesChooser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::rulesChooser)
{
    ui->setupUi(this);
    onRuleAdded();

    //This sends a command once a rule has been selected to the backtester screen
    connect(ui->addBtn, SIGNAL(clicked()),this, SLOT(onAddPressed()));

    //If a rule is deleted then it sends a signal to the backtest screen to tell it to piss off there as well
    connect(ui->deleteBtn, SIGNAL(clicked()),this, SLOT(ruleDeleted()));

    //to recieve command that rules have been created
    QObject::connect(&rule, SIGNAL(newRuleCreated()),
                     this, SLOT(onRuleAdded()));


    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);
    this->setPalette(pal);
    this->setWindowTitle("Pick a Rule");
}

rulesChooser::~rulesChooser()
{
    delete ui;
}

void rulesChooser::onRuleAdded() {

        loadRules();
        ui->ruleList->repaint();
}

void rulesChooser::loadRules() {
    ui->ruleList->clear();

    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
        }
    }
     QString ruleString;
     QString qry;
     QVector<QStringList> rules;

        qry = "SELECT * FROM Rules";

//        //started of random but isnt anymore. it pics the construct foreign key value that relates to the selected foreign key
//        int randomfuckingnumber = (constructs[ui->constructList->selectedItems().at(0)->text().toInt()].at(0)).toInt();
//        qry = qry.arg(randomfuckingnumber - 1); // minus one cause auto index dont start at 0
        rules = db.select(qry);

        for (int i = 0; i < rules.size(); i++) {
            ruleString = "";

            for (int j = 0; j < rules[i].size()-1; j++) {
                ruleString += rules[i].at(j) + " ";
            }

            ui->ruleList->addItem(ruleString);
        }
}

void rulesChooser::on_createBtn_clicked()
{
    rule.open();
}

void rulesChooser::onAddPressed() {
    emit this->sendRule(ui->ruleList->selectedItems().at(0)->text());
    this->close();
}

void rulesChooser::ruleDeleted() {
    emit this->sendDeletedRule(fuckCuntFuck);
}

void rulesChooser::on_backBtn_clicked()
{
    this->close();
}

void rulesChooser::on_deleteBtn_clicked()
{
    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
        }
    }
    int rid = -1;
    //get rid
    QString qry = "SELECT * FROM Rules";
    QVector<QStringList> listOfRules = db.select(qry);

    QString aRule;
    fuckCuntFuck = ui->ruleList->selectedItems()[0]->text();
    for (int i = 0; i < listOfRules.size(); i++) {
        aRule = "";
        for (int j = 0; j < listOfRules[i].size()-1; j++) {
            aRule += listOfRules[i].at(j) + " ";
        }
            if (aRule == fuckCuntFuck) {
                rid = (listOfRules[i].at(0).toInt());
            }
    }

    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
        }
    }
    //delete rule
    qry  = "DELETE FROM rules WHERE ruleID = '%1'";
    qry  = qry.arg(rid);
    db.execSQL(qry);

    //delete relation
    qry  = "DELETE FROM relation WHERE rid = '%1'";
    qry  = qry.arg(rid);
    db.execSQL(qry);

    //repaint
    ui->ruleList->clear();
    loadRules();
}
