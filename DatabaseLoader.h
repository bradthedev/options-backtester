/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DATABASELOADER_H
#define DATABASELOADER_H

#include <QWidget>
#include <QVector>
#include <QStandardItemModel>
#include "database.h"
#include "backtestermainwindow.h"
#include "strategychooser.h"

namespace Ui {
class DatabaseLoader;
}

class DatabaseLoader : public QWidget
{
    Q_OBJECT
    
public:
    explicit DatabaseLoader(QWidget *parent = 0);
    void openWindow();
    QStringList selectFilesDialog();
    QVector<QStringList> readFile(QString file);
    QVector<QStringList> correctData(QVector<QStringList> &filecontents, DB_TABLE table);
    QVector<QStringList> mergeInterestData(QStringList files);
    bool fileToDB(QString filename, DB_TABLE table);
    bool saving;
    void setupTableView(QString filename);
    void loading(bool on);
    void loading(QryString status);
    void updateSize();
    strategychooser stratChooser;
    BacktesterMainWindow backtesterMain;
    ~DatabaseLoader();

private slots:
    void on_btnLoadFile_clicked();
    void on_btnSave_clicked();
    void on_btnContinue_clicked();
    void on_comboFileType_currentIndexChanged(int index);
    void on_btnBack_clicked();
    void on_btnDelete_clicked();

private:
    Ui::DatabaseLoader *ui;
    Database db;
    QStringList selectedFiles;
    DB_TABLE currentTable; //changed via the "file type" dropdown menu
};

#endif // DATABASELOADER_H
