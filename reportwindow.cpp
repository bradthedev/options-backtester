/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reportwindow.h"
#include "ui_reportwindow.h"
#include "report.h"
#include <QFileDialog>
#include <QMessageBox>

ReportWindow::ReportWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReportWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Report");

}

void ReportWindow::reset()
{
    ui->customPlot->removeGraph(0);
}

void ReportWindow::drawGraph(Report report)
{
    this->ui->comboGraph->clear();
    this->report = report;
    report.graph(this->ui->customPlot);
    for(int i = 0; i < this->report.data.size(); i++)
        this->ui->comboGraph->addItem(report.legendNames[i]);
}

ReportWindow::~ReportWindow()
{
    delete ui;
}

void ReportWindow::on_btnSave_clicked()
{
//   Output csv data file
     QString fileName = QFileDialog::getSaveFileName(this, "Save File","*.csv","*.csv");
     if(QFile::remove(fileName))
        QFile::copy("data.txt", fileName);
     else QFile::copy("data.txt", fileName);

//    QString fileName = QFileDialog::getSaveFileName(this, "Save File","","*.txt");
//   this->report.saveAs(fileName,this->ui->comboGraph->currentIndex());
     //   Output txt data file
 //    fileName.replace(QString("csv"), QString("txt"));
//     if(QFile::remove(fileName))
//        QFile::copy("data.txt", fileName);
//     else QFile::copy("data.txt", fileName);

 // Output copy log.txt to xxxxreport.text
      fileName.replace(QString(".csv"), QString("report.txt"));
  //   fileName.replace(QString(".txt"), QString("report.txt"));
     if(QFile::remove(fileName))
        QFile::copy("log.txt", fileName);
     else QFile::copy("log.txt", fileName);

// Output Log and Graph to PDF
    QFile file("log.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    QTextStream in(&file);
    QString text;
    text = in.readAll();
    fileName.replace(QString(".txt"), QString(".pdf"));
    QTextDocument doc;
    doc.setPlainText ( text );
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    printer.A4;
    printer.Landscape;
    doc.print(&printer); // Prints Log

    fileName.replace(QString(".pdf"), QString("graph.pdf"));
    this->ui->customPlot->savePdf(fileName);  // Prints Graph to PDF
    file.close();
    QMessageBox::information(0, "Output Files","Data and Report files saved" );
}
