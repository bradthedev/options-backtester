/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QString>
#include <iostream>
#include <QDebug>

#include "strategies.h"
#include "ui_strategies.h"
#include "strategychooser.h"

strategies::strategies(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::strategies)
{
    ui->setupUi(this);
    QPalette pal = this->palette();

    connect(ui->saveButton, SIGNAL(clicked()),this, SLOT(onStratSavedPressed()));

    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);
    this->setPalette(pal);
    this->setWindowTitle("Enter Strategy Details");
}

strategies::~strategies()
{
    delete ui;
}

void strategies::onStratSavedPressed() {
    emit this->newStratCreated();
}

void strategies::openWindow()
{
    this->show();

    QDate date = QDate::currentDate();
    ui->comboExpire->setDate(date);

    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
    this->startDate = db.selectDate("select min(date) from option");

    updateComboStrike();
}

void strategies::updateComboStrike()
{
    QryString qryStr;
    qryStr = "SELECT DISTINCT strike FROM Option WHERE strike > 0 ORDER BY strike";

    ui->comboStrike->clear();
    ui->comboStrike->insertItems(0,db.selectCol(qryStr));
}

void strategies::on_backButton_clicked()
{
    clearScreen();
    this->close();
}

void strategies::on_saveButton_clicked()
{
    if (ui->nameEdit->text() == "") {
        QMessageBox msgBox;
        msgBox.setText("You must enter a Strategy Name");
        msgBox.exec();
    } else if (ui->tableWidget->rowCount() < 1) {
        QMessageBox msgBox;
        msgBox.setText("You must enter at least one construct");
        msgBox.exec();
    } else {
        QString stratQry = ("INSERT INTO strategies(Name) VALUES ('%1')");
        stratQry = stratQry.arg(ui->nameEdit->text());
        db.execSQL(stratQry);

        QVector<QStringList> stratID;
        QString qry;
        qry = "SELECT StrategyID FROM Strategies WHERE Name = '%1'";
        qry = qry.arg(ui->nameEdit->text());
        stratID = db.select(qry);

        QString constructQry;

            for( int r = 0; r < ui->tableWidget->rowCount(); ++r ){
                constructQry = "INSERT INTO constructs(ExpireDate, ExpireDays, StrikePrice, Type, Action, StrategyID) VALUES ('%1', '%2', '%3', '%4', '%5', '%6')";
                for( int c = 0; c < ui->tableWidget->columnCount(); ++c ){
                    constructQry = constructQry.arg(ui->tableWidget->item( r, c )->text());
                }
                QString construct = stratID.at(0).join("");
                constructQry = constructQry.arg(construct);
                db.execSQL(constructQry);
            }
        clearScreen();
        this->close();
    }
}

void strategies::on_comboExpire_userDateChanged(const QDate &date)
{
    if (ui->comboExpire->date().toString() != "Thu Sep 14 1752") {
        ui->expireDays->setText("0");
    }
}

void strategies::on_expireDays_editingFinished()
{
    QString daysString = ui->expireDays->text();
    QDate minDate = QDate::fromString("14/Sep/1752");
    minDate.setDate(1752, 9, 14);

    ui->comboExpire->setDate(minDate);
}

void strategies::on_addContract_clicked()
{
    ui->tableWidget->setUpdatesEnabled(true);

    int rowCount = ui->tableWidget->rowCount();

    ui->tableWidget->insertRow(rowCount);

    QString expire = "";
    QString days = ui->expireDays->text();
    QString strikePrice = ui->comboStrike->currentText();
    QString action = "";
    QString type = "";

    if (ui->comboExpire->date().toString() != "Thu Sep 14 1752") {
        expire = ui->comboExpire->date().toString();
    } else {
        expire = "0";
    }

    if (ui->radioButton->isChecked()) {
        action = "buy";
    } else {
        action = "sell";
    }

    if (ui->radioButton_3->isChecked()) {
        type = "call";
    } else {
        type = "put";
    }

    QTableWidgetItem *expireItem = new QTableWidgetItem(tr("%1").arg(expire));
    QTableWidgetItem *daysItem = new QTableWidgetItem(tr("%1").arg(days));
    QTableWidgetItem *strikeItem = new QTableWidgetItem(tr("%1").arg(strikePrice));
    QTableWidgetItem *actionItem = new QTableWidgetItem(tr("%1").arg(action));
    QTableWidgetItem *typeItem = new QTableWidgetItem(tr("%1").arg(type));

    ui->tableWidget->setItem(rowCount,0,expireItem);
    ui->tableWidget->setItem(rowCount,1,daysItem);
    ui->tableWidget->setItem(rowCount,2,strikeItem);
    ui->tableWidget->setItem(rowCount,3,actionItem);
    ui->tableWidget->setItem(rowCount,4,typeItem);

}

void strategies::clearScreen() {
    ui->nameEdit->clear();
    ui->comboExpire->clear();
    ui->expireDays->clear();
    while (ui->tableWidget->rowCount() > 0)
    {
        ui->tableWidget->removeRow(0);
    }
}
