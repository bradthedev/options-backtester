#include "aboutwindow.h"
#include "ui_aboutwindow.h"

aboutwindow::aboutwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aboutwindow)
{

    ui->setupUi(this);
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);
    QStringList TellPeopleHowAwesomeWeAre;
    TellPeopleHowAwesomeWeAre.append("Thank you for using MVPs Option Backtester");
    TellPeopleHowAwesomeWeAre.append("This product was brought to you by the MVP Development Teams");
    TellPeopleHowAwesomeWeAre.append("");
    TellPeopleHowAwesomeWeAre.append("Project Manager - Benjamin Hall");
    TellPeopleHowAwesomeWeAre.append("Lead Programmer - Bradley Hunt");
    TellPeopleHowAwesomeWeAre.append("Graphic Designer - Ashley Conroy");
    TellPeopleHowAwesomeWeAre.append("");
    TellPeopleHowAwesomeWeAre.append("");
    TellPeopleHowAwesomeWeAre.append("Option Backtester Version: 2.0");

    ui->listWidget->addItems(TellPeopleHowAwesomeWeAre);

    this->setPalette(pal);
    this->setWindowTitle("About Window");
}

aboutwindow::~aboutwindow()
{
    delete ui;
}

void aboutwindow::on_backButton_clicked()
{
    this->close();
}
