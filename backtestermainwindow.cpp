/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "backtestermainwindow.h"
#include "ui_backtestermainwindow.h"
#include "database.h"
#include <QMessageBox>
#include <iostream>
#include <QTextStream>
#include <cmath>
#include "benchtimer.h"
#include <QDebug>
#include "report.h"

BacktesterMainWindow::BacktesterMainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BacktesterMainWindow)
{
    ui->setupUi(this);
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);

    QObject::connect(&stratChooser, SIGNAL(sendStrategy(QList<QListWidgetItem*>)),
                     this, SLOT(onStrategySelected(QList<QListWidgetItem*>)));

    QObject::connect(&stratChooser, SIGNAL(sendDeletedStrategy(QString)),
                     this, SLOT(onStrategyDeleted(QString)));

    QObject::connect(&rulesAdder, SIGNAL(sendRule(QString)),
                     this, SLOT(onRuleSelected(QString)));

    QObject::connect(&rulesAdder, SIGNAL(sendDeletedRule(QString)),
                     this, SLOT(onRuleDeleted(QString)));


    this->setPalette(pal);
    this->setWindowTitle("Backtester Tool");
}

void BacktesterMainWindow::openWindow()
{
    this->show();
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
      this->startDate = db.selectDate("select min(date) from option");
      ui->dateStart->setDate(this->startDate);
      this->endDate = this->ui->dateEnd->date();
}

void BacktesterMainWindow::onStrategySelected(const QList<QListWidgetItem *> &chosenStrategy) {
    for (int i = 0; i < chosenStrategy.size(); i++) {
        QListWidgetItem* newStrat = new QListWidgetItem( chosenStrategy.front()->text() );

        ui->listWidget->addItem(newStrat);

        loadStrategy(chosenStrategy.front());

        ui->listWidget->item(this->strats.size()-1)->setSelected(true);
        //ui->listWidget->setCurrentRow(this->strats.size());
        ui->listWidget->setCurrentItem(newStrat);

        //setConstuctList(chosenStrategy.front()->text());

        ui->constructList->item(0)->setSelected(true);
        ui->constructList->setCurrentRow(0);
        ui->constructList->setCurrentItem(ui->constructList->item(0));

    }
    ui->listWidget->repaint();
}

void BacktesterMainWindow::onStrategyDeleted(QString delStrat) {

    int evicteeNum = -1;
    for (int i = 0; i < ui->listWidget->count(); i++) {
        if (ui->listWidget->item(i)->text() == delStrat) {
            evicteeNum = i;
        }
    }

    if (evicteeNum >= 0) {
        deleteAStrategy(evicteeNum);
    }
}

void BacktesterMainWindow::onRuleSelected(QString rule) {

    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
        }
    }


    int cid = getCID();
    //Get RuleID
    int rid = getRID(rule.right(rule.size()-2));//of all bugs

    //Add RuleID and ConstructID to relation
    QString qry = "INSERT INTO Relation(cID,rID) VALUES ('%1','%2')";
    qry = qry.arg(cid);
    qry = qry.arg(rid);

    db.execSQL(qry);
    //write results aka all the rules that were selected above according to the CID to the listWidget_2 aka the rules list
    writeRules(cid);

    //loadSelectedRule(rule);
    ui->listWidget_2->repaint();
}

void BacktesterMainWindow::onRuleDeleted(QString delRule) {

    deleteARule(getRID(delRule.right(delRule.size()-2)));
}

void BacktesterMainWindow::writeRules(int cid) {
    ui->listWidget_2->clear();
    QString complexQry = "SELECT DeltaOperator, DeltaValue, ThetaOperator, ThetaValue, VolatilityOperator, VolatilityValue, ExpireValue, Action, Execution, GracePeriod FROM Rules, Relation WHERE Rules.RuleID = Relation.rID AND Relation.cID = '%1'";
    complexQry = complexQry.arg(cid);

    QVector<QStringList> complexResults = db.select(complexQry);
    QString ruleString;

    for (int i = 0; i < complexResults.size(); i++) {
        ruleString = "";
        for (int j = 0; j < complexResults[i].size(); j++) {
            ruleString += complexResults[i][j] + " ";
        }
        ui->listWidget_2->addItem(ruleString);
    }
}

int BacktesterMainWindow::getCID() {

    QString cur = constructIDList[ui->listWidget->row(ui->listWidget->currentItem())].at(ui->constructList->row(ui->constructList->currentItem()));

    return cur.toInt();
}

int BacktesterMainWindow::CIDget(QString constructString) {

    QString cur = constructString;
    QStringList curAtt = cur.split(" ");

    QString date;

    if (curAtt[0] != "0") {
        for (int i = 0; i < 4; i++) {
            date += curAtt[0] + " ";
            curAtt.removeFirst();
        }
        date = date.trimmed();
    } else {
        date = "0" ;
        curAtt.removeFirst();
    }

    QString findConsQry = "SELECT ConstructID FROM Constructs WHERE ExpireDate = '%1' AND ExpireDays = '%2' AND StrikePrice = '%3' AND Type = '%4' AND Action = '%5'";
     findConsQry = findConsQry.arg(date);

    for (int i = 0; i < curAtt.size()-1; i++) {
        findConsQry = findConsQry.arg(curAtt[i]);
    }

    QVector<QStringList> CID = db.select(findConsQry);

    return CID[0].at(0).toInt();
}


int BacktesterMainWindow::getRID(QString rule) {

    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");

    QString qry = "SELECT * FROM Rules";
    QVector<QStringList> listOfRules = db.select(qry);

    QString aRule;

    for (int i = 0; i < listOfRules.size(); i++) {
        aRule = "";
        for (int j = 1; j < listOfRules[i].size()-1; j++) {
            aRule += listOfRules[i].at(j) + " ";
        }
            if (aRule == rule) {
                return (listOfRules[i].at(0).toInt());
            }
    }

    return -1;
}

void BacktesterMainWindow::loadStrategy(QListWidgetItem *theStrategy) {

    if(!db.db.isOpen()){
        if(!db.openDB()){ //opens or creates the database
            qDebug() << "db.openDB error:" << db.db.lastError();
        }
    }

     QString stratName = theStrategy->text();

     QString qry = "SELECT * FROM Constructs, Strategies WHERE Strategies.StrategyID = Constructs.StrategyID AND Strategies.Name = '%1'";
     qry = qry.arg(stratName);

     QVector<QStringList> vals;
     vals = db.select(qry);

     int i = 0;
     strats.append(stratName);

     QStringList constructIDs;
     QStringList expire;
     QStringList days;
     QStringList strike;
     QStringList action;
     QStringList type;

     //QString constuctString;

     while ( i < vals.size()) {
         constructIDs.append(vals.at(i).value(0));
         expire.append(vals.at(i).value(1));
         days.append(vals.at(i).value(2));
         strike.append(vals.at(i).value(3));
         action.append(vals.at(i).value(4));
         type.append(vals.at(i).value(5));
         i++;

     }
     constructIDList.append(constructIDs);
     expireList.append(expire);
     daysList.append(days);
     strikeList.append(strike);
     actionList.append(action);
     typeList.append(type);

}

void BacktesterMainWindow::buyOptions(double &capital, OPTION &option, QString type)
{
    if (type == "put") {
        option.count = (int)capital/option.strike;
        capital = capital + (double(option.count)*option.ask);
    } else {
        option.count = (int)capital/option.strike;
        capital = capital - (double(option.count)*option.ask);
    }

}

BacktesterMainWindow::~BacktesterMainWindow()
{
    delete ui;
}

/* This is insane amount of shitty last minute programming. Not by me I am better
 * than that... ok a little of me a little from the group before. This is the bulk
 * of the logic that runs the backtester. Contact me and I can help explain what I know
 * also the previous group discarded the majority of data and only looks at one option a
 * day! As far as I can tell this is incorrect and makes everything else wrong :S
 * PS that isn't the worst logic I found but I fixed the rest!
 */
void BacktesterMainWindow::on_btnRun_clicked()
{
    if (ui->listWidget->count() < 1 || ui->constructList->count() < 1) {
        QMessageBox msgBox;
        msgBox.setText("You must choose a valid strategy");
        msgBox.exec();
    } else {
        int stratCount = 0;
        report.data.clear();
        report.legendNames.clear();
        QFile::remove("log.txt");
        QFile file("log.txt");
        QFile::remove("data.txt");
        QFile file2("data.txt");

        QTextStream ss(&file);
        QTextStream data(&file2);

        double startingCash = this->ui->spinStartMoney->value();

        //this loops for every strategy that is in the strategy list
        for (int x = 0; x < ui->listWidget->count(); x++) { // replace 1 with ui->listWidget->count()
          file.open(QIODevice::WriteOnly | QIODevice::Text);
          file2.open(QIODevice::WriteOnly | QIODevice::Text);
          data << "StartDate"<< "," << "Money" << "," << "x" << "\n";

          //Starting Cash gets reset for each strategy
          double money = startingCash;//this->ui->spinStartMoney->value();

            //this should loop for each construct but not really sure how that logic works so just does the first
            //construct in the list. Good luck to whoever makes it work for multiple
            for (int y = 0; y < 1; y++) { //replace 1 with expireList[x].size()

                setOffAskUnder(y, x);

                int consID = constructIDList[x].at(y).toInt();
                //get rules that link to current construct
                QString consQry = "SELECT rules.*, relation.cID FROM rules JOIN Relation ON rID = ruleID AND relation.cID = '%1'";
                consQry = consQry.arg(consID);
                QVector<QStringList> listOfRules = db.select(consQry);

                this->setEnabled(false);
                qApp->processEvents();
                OPTION option;
                QVector<QStringList> optionValues;
                option.strike = strikeList[x].value(y).toDouble();
                option.ask = this->ui->txtAsk->text().toDouble();
                buyOptions(money,option, typeList[x].at(y));

                //calculateValidExpiry(QDate::fromString(this->expireList[x][y]), option.strike, y, x);
                //QDate d = calculateValidExpiry(this->expireList[x][y], QString::number(option.strike), y, x);//QDate::fromString(this->expireList[x][y]);

                if (this->expireList[x][y] != 0) {
                    option.expDate = calculateValidExpiry(this->expireList[x][y], QString::number(option.strike), y, x);
                } else {
                    option.expDate = calculateValidExpiry(this->startDate.addDays(daysList[x][y].toInt()).toString(), QString::number(option.strike), y, x);
                }


                int months;

                if (daysList[x].value(0).toInt() != 0) {
                    months = daysList[x].value(y).toInt()/30;
                } else {
                    months = diffMonths(this->startDate,QDate::fromString(expireList[x].value(y),DATE_FORMAT));
                }

                ss << "Start date: " << this->startDate.toString(DATE_FORMAT) << "\n"
                   << "End date: " << this->endDate.toString(DATE_FORMAT) << "\n"
                   << "Starting Money: $" << this->ui->spinStartMoney->value() << "\n\n";

                //money -= this->ui->txtAsk->text().toDouble() * option.count;

                ss << "Starting Purchase:\n"
                         << " Options: " << option.count << "\n"
                         << " Strike:  " << option.strike << "\n"
                         << " Expire:  " << option.expDate.toString(DATE_FORMAT) << "\n"
                         //Edited for GUI << " Ask:     " << this->ui->txtAsk->text().toDouble() << "\n"
                         << "Money: $" << money << "\n\n";

                //removed and ExpireDate = '%2' just for shits and gigs also cause it wasn't returning anything with it
                QryString qry = "select bid from Option where date = '%1'  and strike = '%2' and type = '%3'";
                qry = qry.arg(this->startDate);
                //qry = qry.arg(option.expDate);
                qry = qry.arg(option.strike);
                qry = qry.arg(typeList[x].at(y));

                double bid = db.selectSingle(qry).toDouble();
                this->report.addRecord(this->startDate,money+(option.count * ((this->ui->txtAsk->text().toDouble()+bid)/2)),x);
                data << this->startDate.toString(DATE_FORMAT) << "," << money+(option.count * ((this->ui->txtAsk->text().toDouble()+bid)/2)) << "," << x << "\n";
                //grab all of the option pricing for above strike and expiry
                if(this->ui->boolDaily->isChecked()){
                    qry = "select date, bid, ask from option where ExpireDate = '%1' and strike = '%2' and type = '%3'";
                    qry = qry.arg(option.expDate);
                    qry = qry.arg(option.strike);
                    qry = qry.arg(typeList[x].at(y));
                    optionValues = db.select(qry);
                }
                //delta, theta and all that shit.

                for(QDate date = this->startDate; date < this->endDate; date = date.addDays(1)){
                    qry = "SELECT delta, theta FROM option WHERE Date = '%1' and strike = '%2' and type = '%3'";
                    qry = qry.arg(date);
                    qry = qry.arg(option.strike);
                    qry = qry.arg(typeList[x].at(y));
                    QVector<QStringList> extraValues = db.select(qry);


                    bool ruleSuccess = false;
                    bool dateSuccess = false;
                    QString ruleResult = "null";
                    if (extraValues.size() > 0) {
                        if (ui->listWidget_2->count() >0) {
                            ruleResult = exitRule(option, this->startDate, date, listOfRules);
                        }
                        option.delta = extraValues[0][0].toInt();
                        option.theta = extraValues[0][1].toInt();
                        if (ruleResult != "null") {
                            ruleSuccess = true;
                        }
                    }
                    if (date >= option.expDate) {
                        dateSuccess = true;
                    }
                    if(dateSuccess || ruleSuccess ){
                    //after or on expiry or rule has been hit
                        if(option.count != 0){
                        //if options haven't been excersized yet
                            QryString qstr = "SELECT Price FROM Stock WHERE Date = '%1'";
                            qstr = qstr.arg(date);
                            QString currentPrice = db.selectSingle(qstr);
                            if(currentPrice.toDouble()>option.strike){
                                ss << date.toString(DATE_FORMAT) << "\n"
                                         << option.count << " options were excercised in the money.\n"
                                         << " Strike: $" << option.strike << "\n"
                                         << " Current price: $" << currentPrice.toDouble() << "\n";


                                if (dateSuccess) {
                                    if (option.type == "put") {
                                        money -= (currentPrice.toDouble()-option.strike)*option.count;
                                    } else {
                                        money += (currentPrice.toDouble()-option.strike)*option.count;
                                    }
                                } else {
                                    if (ruleResult == "buy") {
                                        money -= (currentPrice.toDouble()-option.strike)*option.count;
                                    } else {
                                        money += (currentPrice.toDouble()-option.strike)*option.count;
                                    }
                                }



                            }else{
                                ss << date.toString(DATE_FORMAT) << "\n"
                                         << option.count << " options expired out of the money.\n"
                                         << " Strike: $" << option.strike << "\n"
                                         << " Current price: $" << currentPrice.toDouble() << "\n";
                            }

                            ss << "Money: $" << money << "\n\n";

                            this->report.addRecord(date,money,x);
                            data << date.toString(DATE_FORMAT) << "," << money << "," << x << "\n";
                            option.count = 0;
                        }

                        if(db.count(QryString("select count(*) from option where date = '%1' and type='%2'").arg(date).arg(typeList[x].at(y)))>0){
                        //if there are available options for this date
                            option = db.selectClosestOption(date,months,this->strikePOffset,typeList[x].at(y));

                            if(option.expDate > this->endDate)
                                break;

                            buyOptions(money,option, typeList[x][y]);

                            ss << date.toString(DATE_FORMAT) << "\n"
                                     << "Purchased:   " << option.count << " options. " << "\n"
                                     << " Strike:     " << option.strike << "\n"
                                     << " Underlying: " << option.underlying << "\n"
                                     << " Expiring:   " << option.expDate.toString(DATE_FORMAT) << "\n"
                                     << " Ask:        " << option.ask << "\n"
                                     << "Money: $" << money << "\n\n";

                            ///Grab all of the option pricing for above strike and expiry
                            ///This saves a ton of disc reads since it's only done once every expiry instead of every day.

                            if(this->ui->boolDaily->isChecked()){
                                QryString qry = "select date, bid, ask from option where ExpireDate = '%1' and strike = %2 and type = '%3'";
                                qry = qry.arg(option.expDate);
                                qry = qry.arg(option.strike);
                                qry = qry.arg(typeList[x].at(y));
                                optionValues = db.select(qry);
                            }
                        }
                    } else { //non-action day
                        if(date == this->startDate)
                            continue; //this day is already logged.

                        if(this->ui->boolDaily->isChecked()){
                            if(option.count==0)
                                ss << "error?";

                            ///Find the correct option price from optionValues
                            int index = -1;
                            QString sdate = date.toString(DATE_FORMAT);

                            for(int i = 0; i < optionValues.size(); i++) {
                                if(optionValues[i][0]==sdate) {
                                    if(index == -1 || optionValues[i][1]>optionValues[index][1]) //grab the one with the biggest bid if multiple on same date
                                        index = i;
                                }
                            }

                            if(index == -1){//no dates matched, skip logging this day
                                qDebug() << date;
                                continue;
                            }

                            if(optionValues[index][1].toDouble()+optionValues[index][2].toDouble()==0 && option.expDate.addDays(-1) == QDate::fromString(optionValues[index][0],DATE_FORMAT))
                                continue; //occasionally, on the last day before expiry, the bid and ask prices are 0. These create wierd spikes so they are skipped.

                            ///Add worth to the graph
                            double price = (optionValues[index][1].toDouble() + optionValues[index][2].toDouble())/2;
                            this->report.addRecord(date,money+(option.count * price),x);
                            data << date.toString(DATE_FORMAT) << "," << money+(option.count * price) << "," << x << "\n";
                            if(this->ui->boolLogDaily->isChecked())
                                ss << "\nDay:" << date.toString(DATE_FORMAT) << " Total worth:" << money+(option.count * price) << " Bid:" << optionValues[index][1] << " Ask:" << optionValues[index][2] << "\n";

            //                if(optionValues[index][1].toDouble()+optionValues[index][2].toDouble()!=0){
            //                    worth += option.count * price;
            //                    this->report.addRecord(date,worth,x);
            //                }

                        }
                    }
                }

            }
        ss << "end backtest\n";

        this->report.addLegend(this->strats.at(x));
        stratCount = x;

        file.close();
        file2.close();
      }
        QryString stockQry;
        stockQry = "select date, price from stock where date > '%1' and date < '%2' order by date asc";
        stockQry = stockQry.arg(this->startDate).arg(this->endDate);


        //Must Have Stock Data Entered Checker
        QVector<QStringList> baseline = db.select(stockQry);
        if (baseline.size() < 1) {
            QMessageBox stockBox;
            stockBox.setText("You must enter Stock Data before continuing");
            stockBox.exec();
            this->setEnabled(true);
            this->close();
            return;
        } else {
        double stockQuant = this->ui->spinStartMoney->value()/baseline[0][1].toDouble();

        for (int i = 0; i < baseline.size(); i++)
            this->report.addRecord(QDate::fromString(baseline[i][0],DATE_FORMAT),baseline[i][1].toDouble()*stockQuant,stratCount+1);
        this->report.setBaseline(1);
        this->report.addLegend("Baseline");
        this->report.saveAs("TestReport.csv",0);
        this->reportWindow.show();
        this->reportWindow.reset();
        this->reportWindow.drawGraph(report);
        this->setEnabled(true);
        qApp->processEvents();
        //bench.display("backtest finished: ");
        }
    }
}

QString BacktesterMainWindow::exitRule(OPTION o, QDate startD, QDate curDate, QVector<QStringList> listOfRules) {
    QStringList currentRule;
    for (int i = 0; i < listOfRules.size(); i++) {
        currentRule = listOfRules[i];

        if (startD.daysTo(curDate) > currentRule[10].toInt()) {
            if (currentRule[1] != "n") {
                if (getResult(o.delta,currentRule[1],currentRule[2].toInt())) {
                    return currentRule[8];
                }
            }
            if (currentRule[3] != "n") {
                if (getResult(o.theta,currentRule[3],currentRule[4].toInt())) {
                    return currentRule[8];
                }
            }
            if (currentRule[7] != "null") {
                if (curDate.addDays(currentRule[7].toInt()) == o.expDate) {
                    return currentRule[8];
                }
            }
        }
    }

    return "null";
}

bool BacktesterMainWindow::getResult(int a, QString operat, int b) {
    char op = operat.at(0).unicode();

    switch (op) {
        case '<':
            return a < b;
        case '<=':
            return a <= b;
        case '>':
            return a > b;
        case '>=':
            return a >= b;
        case '=':
            return a == b;
        case '!=':
            return a != b;
    }

    return false;
}

void BacktesterMainWindow::setOffAskUnder(int index, int stratIndex) {

    QDate validExpire;
    QDate expireAsDate = QDate::fromString(expireList[stratIndex].value(index));
    if (daysList[stratIndex].value(index).toInt() != 0) {
        validExpire = calculateValidExpiry((ui->dateStart->date().addDays(daysList[stratIndex].value(index).toInt()).toString()), strikeList[stratIndex].value(index), index, stratIndex);
    } else {
        validExpire = calculateValidExpiry(expireAsDate.toString(), strikeList[stratIndex].value(index), index, stratIndex);
    }



    QryString qryStr = "select OptionID from Option where date = '%1' and ExpireDate = '%2' and type = '%3' and strike = %4";
    qryStr = qryStr.arg(this->startDate).arg(validExpire).arg(typeList[stratIndex].at(index)).arg(strikeList[stratIndex].value(index).toDouble());
    int ID = db.selectSingle(qryStr).toInt();
    OPTION option = db.selectOption(ID);
    double offset = option.strike - option.underlying;
    double percentOffset = 1-(option.underlying/option.strike);
    this->ui->txtOffset->setText(QString::number(offset)+ " (" + QString::number(percentOffset*100,'f',1) + "%)");
    this->ui->txtPrice->setText(QString::number(option.underlying));
    this->ui->txtAsk->setText(QString::number(option.ask));
    this->strikePOffset = percentOffset;

}

QDate BacktesterMainWindow::calculateValidExpiry(QString expire, QString strike, int currentIndex, int stratIndex) {
    QryString qryStr;
    QStringList results;

    //Get the closest valid date that matches the type and strike
    qryStr = "SELECT DISTINCT(ExpireDate),date FROM Option WHERE date >= '%1' AND type='%2' AND strike='%3' ORDER BY date";
    qryStr = qryStr.arg(ui->dateStart->date()).arg(typeList[stratIndex].at(currentIndex)).arg(strike);
    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
    results = db.selectCol(qryStr);

    QString temp;
    temp = results.takeFirst();
    temp.remove('-');

    QDate resultDate = QDate::fromString(temp, "yyyyMMdd");//results.at(index) instead of temp
    resultDate.setDate(resultDate.year(),resultDate.month(),resultDate.day());
    return resultDate;
}

void BacktesterMainWindow::on_dateStart_userDateChanged(const QDate &date)
{
    this->startDate = date;
    updateComboExpire();
    updateComboStrike();
}

void BacktesterMainWindow::on_dateEnd_userDateChanged(const QDate &date)
{
    this->endDate = date;
}

void BacktesterMainWindow::on_btnQuit_clicked()
{
    this->close();
}

void BacktesterMainWindow::on_pushButton_2_clicked()
{
    this->stratChooser.openWindow();
}

void BacktesterMainWindow::on_pushButton_5_clicked()
{
    this->rulesAdder.open();
}

void BacktesterMainWindow::on_pushButton_3_clicked()
{
   int stratRow;

   if (ui->listWidget->selectedItems().size() > 0){
      stratRow = ui->listWidget->row(ui->listWidget->selectedItems().at(0));
      deleteAStrategy(stratRow);
   }
}

void BacktesterMainWindow::deleteAStrategy(int stratRow) {
    this->strats.removeAt(stratRow);
    this->constructIDList.removeAt(stratRow);
    this->expireList.removeAt(stratRow);
    this->daysList.removeAt(stratRow);
    this->strikeList.removeAt(stratRow);
    this->actionList.removeAt(stratRow);
    this->typeList.removeAt(stratRow);
    int size = ui->listWidget->count();
    delete ui->listWidget->takeItem(stratRow);
    ui->constructList->clear();
}

void BacktesterMainWindow::on_listWidget_currentTextChanged(const QString &currentText)
{
    if (currentText != "") {
        ui->listWidget_2->clear();
        setConstuctList(currentText);

        ui->constructList->item(0)->setSelected(true);
        ui->constructList->setCurrentRow(0);
        ui->constructList->setCurrentItem(ui->constructList->item(0));

        writeRules(getCID());
        ui->pushButton_5->setEnabled(true);
    }
}

void BacktesterMainWindow::setConstuctList(const QString &currentText) {
    ui->constructList->clear();
    QString stratName = currentText;

    int index = strats.indexOf(stratName);

    QString constuctString;
    int i = 0;
    while ( i < expireList[index].size()) {
            constuctString = "";

            constuctString += expireList[index].at(i) + " ";

            constuctString += daysList[index].at(i) + " ";

            constuctString += strikeList[index].at(i) + " ";

            constuctString += actionList[index].at(i) + " ";

            constuctString += typeList[index].at(i) + " ";

            ui->constructList->addItem(constuctString);
            i++;

    }
}

/*Shit code from previous group. DOES NOTHING AT ALL!!!!!!!
* But the program crashes when you remove it and I am out of
* time to remove and stuff (it's the night before presentation)
* So in advanced thanks to future group for fixing
*/
void BacktesterMainWindow::updateComboStrike()
{
    QryString qryStr;
    qryStr = "select strike from Option where date = '%1' and expiredate = '%2' and type='call'";
}

void BacktesterMainWindow::updateComboExpire()
{
    QryString qryStr;
    qryStr = "select distinct(ExpireDate) from Option where date = '%1' and type='call'";
    qryStr = qryStr.arg(ui->dateStart->date());
}

int BacktesterMainWindow::diffMonths(QDate start, QDate end)
{
    int mDiff = end.month() - start.month();
    //int dDiff = end.toJulianDay() - start.toJulianDay();
    int yDiff = end.year() - start.year();
    //if(dDiff>365)
    mDiff += 12*yDiff;
    return mDiff;
}
//-------------END OF USELESS CODE-----------------//

void BacktesterMainWindow::on_constructList_itemClicked(QListWidgetItem *item)
{
    ui->listWidget_2->clear();
    writeRules(getCID());
    ui->pushButton_5->setEnabled(true);
    setOffAskUnder(ui->constructList->currentIndex().row(), ui->listWidget->currentIndex().row());
}

void BacktesterMainWindow::on_pushButton_6_clicked()
{
    //get rid
    int rid = getRID(ui->listWidget_2->selectedItems().at(0)->text());
    deleteARule(rid);

}

void BacktesterMainWindow::deleteARule(int rid) {

    if(!db.db.isOpen())
        if(!db.openDB())
            QMessageBox::information(0, "Error", "Database did not open correctly!");
    //delete from relation
    QString qry = "DELETE FROM relation WHERE rid = '%1'";
    qry = qry.arg(rid);
    db.execSQL(qry);

    //repaint
    ui->listWidget_2->clear();
    writeRules(getCID());
}
