/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include "DatabaseConstants.h"
#include "qrystring.h"

class Database : public QObject
    {
    public:
        Database(QObject *parent = 0);
        bool openDB();
        bool execSQL(QryString qryStr);
        bool setup();
        bool isSetup();
        bool loadData(QVector<QStringList> &fileContents, DB_TABLE table);
        int count(QryString qryStr);
        OPTION selectClosestOption(QDate date, int months, double strikePOffset, QString type);
        OPTION selectOption(int ID);
        QString selectSingle(QryString qryStr);
        QStringList selectCol(QryString qryStr);
        QVector<QStringList> select(QryString qryStr);
        QDate selectDate(QryString qryStr);
        QString formatDate(QString strDate);
        QString formatDate(QDate date);
        double getDBSize();
        void clear();
        ~Database();


        QSqlDatabase db;
    };

#endif // DATABASE_H
