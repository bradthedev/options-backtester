/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "database.h"
#include "DatabaseConstants.h"
#include "benchtimer.h"
#include <QMessageBox>
#include <iostream>
#include <QDir>
#include <QSqlQuery>
#include <QVariant>
#include <cmath>
#include <QDebug>
#include <QSqlRecord>

Database::Database(QObject* parent)
{
}

Database::~Database()
{
     QString connection;
     connection=db.connectionName();
     db.close();
     db = QSqlDatabase(); // Removes the reference to the original database (should fix any errors when deleting the database)
     QSqlDatabase::removeDatabase(connection);
}

//Non-query SQL (no return data)
bool Database::execSQL(QryString qryStr)
{
    if (db.isOpen())
    {
        QSqlQuery query;
        //QMessageBox::information(0, "error", query.lastError().text());

        return query.exec(qryStr);

    }
    return false;
}

//////////////////
///Returns the fist selected column in a qvector
///TODO: check if table and field are single fields
QStringList Database::selectCol(QryString qryStr)
{
    QSqlQuery query;
    QStringList results;
    if (db.isOpen())
    {

        //QMessageBox::information(0, "error", query.lastError().text());
        if(!query.exec(qryStr))
            return results;

        while(query.next())
            results.push_back(query.value(0).toString());

        return results;

    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));
    return results;
}

QVector<QStringList> Database::select(QryString qryStr)
{
    QSqlQuery query;
    QVector<QStringList> results;
    if (db.isOpen())
    {
        //QMessageBox::information(0, "error", query.lastError().text());
        if(!query.exec(qryStr))
            return results;
        while(query.next()){
            QStringList line;
            for(int i = 0; i < query.record().count(); i++)
                line.push_back(query.value(i).toString());
            results.push_back(line);
        }

        return results;

    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));
    return results;
}

QDate Database::selectDate(QryString qryStr)
{
    return QDate::fromString(selectSingle(qryStr),DATE_FORMAT);
}

bool Database::openDB()
{
    db = QSqlDatabase::addDatabase("QSQLITE");

    QString path(QDir::home().path());
    path.append(QDir::separator()).append(DB_FILENAME);
    path = QDir::toNativeSeparators(path);
    //QFile::remove(path); //For Testing only!
    db.setDatabaseName(path);
    db.setConnectOptions("foreign_keys = ON");
    db.setConnectOptions("PRAGMA foreign_keys = ON");
    return db.open();
}

//Create tables needed for the backtester.
//Doesn't cause a problem if run twice since queries will simply fail on the second time.
bool Database::setup()
{
    if (db.isOpen())
    {
        //execSQL("drop table Option");
        execSQL(DB_OPTION_TABLE_SCHEMA);
        execSQL(DB_INTEREST_TABLE_SCHEMA);
        execSQL(DB_STOCK_TABLE_SCHEMA);

        execSQL(DB_STRATEGIES_TABLE_SCHEMA);
        execSQL(DB_CONSTRUCTS_TABLE_SCHEMA);
        execSQL(DB_RULES_TABLE_SCHEMA);
        execSQL(DB_RELATION_TABLE_SCHEMA);

        execSQL(DB_OPTION_INDEX_DATE);
        execSQL(DB_OPTION_INDEX_EXPDATE);
        execSQL(DB_STOCK_INDEX_DATE);
        return true;
    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));
    return false;
}

//TODO: actually check if it's setup
bool Database::isSetup()
{
    return false;
}

bool Database::loadData(QVector<QStringList> &fileContents, DB_TABLE table)
{
    if (db.isOpen())
    {
        TABLE_META tableMeta = TABLE_META(DB_META[table]);

        std::vector<int> columTarget = tableMeta.colTarget;
        QString baseqry = QString("insert into %1 values(NULL").arg(tableMeta.name); //base query
        int i, j;

        if(table == Stock)
            baseqry += ", 'SPX'";

        /////////////
        /// Builds baseqry into the query that will handle each insert.
        /// The replace symbols (%+number) set the order that the .arg calls will replace placeholders
        /// Finished example:
        ///     insert into Option values
        ///         (NULL, '%1',  %2 , '%3', '%5', '%4',  %6 ,  %7 ,  %8 ,  %9 ,  %10 ,  %11 ,  %12 ,  %13 ,  %14 ,  %15 , datetime('now'))
        for(i = 0; i < columTarget.size(); i++) //for each columTarget[] value
            if(columTarget[i] != 0) //ignore columns that don't go into DB
                baseqry += ", " + surround[tableMeta.colType[i]][0] + "%" + QString::number(columTarget[i]) + surround[tableMeta.colType[i]][1];
        baseqry.append(", datetime('now'))"); //currentdate column is always last
        //std::cout << baseqry.toStdString() << std::endl;
        /////////////
        /// Builds an insert query by replacing baseqry % symbols and executes it.
        /// Wrapped in a transaction for a signifigant performance boost.
        //BenchTimer timer;
        QString qry;
        db.transaction();
        for(i = 1; i < fileContents.size(); i++){ //for each csv row
            qry = baseqry;
            for(j = 0; j < fileContents[1].size(); j++) //for each csv col
                if(columTarget[j] != 0) //ignore columns that don't go into DB
                    if(tableMeta.colType[j]!=2)
                        qry = qry.arg(fileContents[i][j]);
                    else //date
                        qry = qry.arg(formatDate(fileContents[i][j]));
            //std::cout << qry.toStdString() << std::endl;
            execSQL(qry);
        }
        //std::cout << baseqry.toStdString() << std::endl;
        db.commit();
        //timer.display("insert");
    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));

    return true;
}

int Database::count(QryString qryStr)
{
    return selectSingle(qryStr).toInt();
}

///////////////////////////////////////////
/// Selects the Option record closest to the specified fields
/// date: selects only options on this date
/// type: selects only options of this type
/// months: selects records with an expiry of this many months past date (closest available)
/// strikeOffset: select the record that matches the above and has the closest strike to price+strikeOffset
///
///////////////////////////////////////////
OPTION Database::selectClosestOption(QDate date, int months, double strikePOffset, QString type)
{
    OPTION option;
    QryString qryStr;
    QSqlQuery query;
    if (db.isOpen())
    {
        option.date = date;
        option.type = type;

        ///////////////
        /// Find the closest Expiration to date + months
        qryStr = "select ExpireDate from Option"
                 " where Date = '%1'"
                 " order by abs(julianday(ExpireDate) - julianday('%2'))"
                 " asc limit 1";
        qryStr = qryStr.arg(date).arg(date.addMonths(months));

        //qDebug() << qryStr << "\n";

        //QMessageBox::information(0, "error", query.lastError().text());
        if(!query.exec(qryStr))
            return option;

        query.next();
        option.expDate = QDate::fromString(query.value(0).toString(),DATE_FORMAT);


        ///////////////
        /// Find the closest strike
        qryStr = "select Strike, LastPrice, Bid, Ask from Option"
                 " where Date = '%1' and ExpireDate = '%2' and type = 'call'"
                 " order by abs(1-(LastPrice/Strike) - %3)"
                 " asc limit 1";
        qryStr = qryStr.arg(date).arg(option.expDate).arg(strikePOffset);

        if( !query.exec(qryStr))
            return option;


        query.next();
        option.strike = query.value(0).toDouble();
        option.underlying = query.value(1).toDouble();
        option.bid = query.value(2).toDouble();
        option.ask = query.value(3).toDouble();
        //qDebug() << (QString)qryStr << "\n" << option.strike << option.underlying << 1-(option.underlying/option.strike) << strikePOffset;
        return option;

    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));
    return option;
}


OPTION Database::selectOption(int ID)
{
    OPTION option;
    if (db.isOpen()){
        QryString qryStr = "select Strike, Bid, Ask, Date, ExpireDate, Type from Option where OptionID = %1";
        qryStr = qryStr.arg(ID);
        QSqlQuery query;
        if(!query.exec(qryStr)){
            QMessageBox::information(0, "exec error", query.lastError().text());
            return option;
        }
        if(query.next()){
            option.strike = query.value(0).toDouble();
            option.bid = query.value(1).toDouble();
            option.ask = query.value(2).toDouble();
            option.date = QDate::fromString(query.value(3).toString(),DATE_FORMAT);
            option.expDate = QDate::fromString(query.value(4).toString(),DATE_FORMAT);
            option.type = query.value(5).toString();
        }else{
            //QMessageBox::information(0, "no query results error", query.lastError().text());
            return option;
        }
        qryStr = "select Price from Stock where date = '%1'";
        qryStr = qryStr.arg(option.date.toString(DATE_FORMAT));
        option.underlying = selectSingle(qryStr).toDouble();
    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));
    return option;
}

QString Database::selectSingle(QryString qryStr)
{
    //qryStr "select OptionID from Option where date = '%1' and ExpireDate = '%2' and type = 'call' and strike = %3";
    QSqlQuery query;
    QString result = "";
    if (db.isOpen()){
        //QMessageBox::information(0, "error", query.lastError().text());
        if(!query.exec(qryStr))
            return result;

        if(query.next())
            result = query.value(0).toString();

        return result;

    }else
        QMessageBox::information(0, "database is closed", QString(db.isOpen()));
    return result;
}

//TODO improve this function (add support for more input date formats, use qdate?)
QString Database::formatDate(QString strDate)
{
    QStringList sList = strDate.split("/");
    if(sList.size()<2)
        return "";
    return sList[2]+ "-" + sList[0].rightJustified(2, '0') + "-" + sList[1].rightJustified(2, '0');
}
QString Database::formatDate(QDate date)
{
    return date.toString(DATE_FORMAT);
}

double Database::getDBSize() //in megabytes
{
    QString path(QDir::home().path());
    path.append(QDir::separator()).append(DB_FILENAME);
    path = QDir::toNativeSeparators(path);
    QFile dbfile(path);
    dbfile.open(QFile::ReadOnly);
    double i = dbfile.size();
    dbfile.close();
    return i*0.000000953674;
}

//Deletes the old database and creates a new empty one.
//outputs an error message to qdebug for some reason. but the function still works
void Database::clear()
{
    QString connection;
    connection=db.connectionName();
    db.close();
    db = QSqlDatabase(); // Removes the reference to the original database (should fix any errors when deleting the database)
    QSqlDatabase::removeDatabase(connection);


    QString path(QDir::home().path());
    path.append(QDir::separator()).append(DB_FILENAME);
    path = QDir::toNativeSeparators(path);
    QFile dbfile(path);
    //qDebug() << "deleting file" << path << "\n";
    dbfile.remove();
    dbfile.close();

    openDB();
    setup();
}
