/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDesktopServices"
#include "QDir"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QPalette pal = this->palette();
    pal.setColor(QPalette::Window, Qt::white);
    //this->setWindowFlags(Qt::Popup);
    this->setPalette(pal);
    this->setWindowTitle("MVP - Backtesting Tool");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnBacktester_clicked()
{
    this->dbloaderMain.openWindow();
}

void MainWindow::on_btnReports_clicked()
{
    this->reportsMain.show();
}


// Main Menu Options
void MainWindow::on_newStragety_triggered()
{
     this->strategiesMain.openWindow();
}

void MainWindow::on_actionExit_triggered()
{
     exit(0);
}

void MainWindow::on_manual_triggered()
{
    QString dirname = QDir::currentPath();
    QDesktopServices::openUrl(QUrl("file:///" + dirname + "/userManual.pdf",QUrl::TolerantMode));

}

void MainWindow::on_about_triggered()
{
    this->aboutwindowMain.show();
}
