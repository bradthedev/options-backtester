/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef DATABASECONSTANTS_H
#define DATABASECONSTANTS_H

#include <vector>
#include <QDate>

const QString DB_FILENAME = "BacktesterDB.sqlite";
enum DB_TABLE{Option,Interest,Stock,Strategies,Constructs,Relation,Rules,DB_TABLE_COUNT}; //DB_TABLE_COUNT must be at the end.

/////////////////////////////////////////////
///   Table creation strings
/////////////////////////////////////////////
const QString DB_OPTION_TABLE_SCHEMA =
        "create table Option"
        "(OptionID      integer primary key,"
        " Name          varchar(10),"
        " LastPrice     double,"
        " Type          varchar(4),"
        " Date          date,"
        " ExpireDate    date,"
        " Strike        double,"
        " Bid           double,"
        " Ask           double,"
        " Volume        integer,"
        " Interest      double,"
        " IV            double,"
        " Delta         double,"
        " Gamma         double,"
        " Theta         double,"
        " Vega          double,"
        " InsertDate    date)";

const QString DB_INTEREST_TABLE_SCHEMA =
        "create table Interest"
        "(InterestID    integer primary key,"
        " Date          date,"
        " Rate1m        double,"
        " Rate3m        double,"
        " Rate6m        double,"
        " Rate9m        double,"
        " Rate12m       double,"
        " Rate15m       double,"
        " Rate18m       double,"
        " Rate21m       double,"
        " Rate24m       double,"
        " Rate27m       double,"
        " InsertDate    date)";

const QString DB_STOCK_TABLE_SCHEMA =
        "create table Stock"
        "(StockID       integer primary key,"
        " Name          varchar(10),"
        " Date          integer,"
        " Price         double,"
        " InsertDate    date)";

const QString DB_STRATEGIES_TABLE_SCHEMA =
        "create table Strategies"
        "(StrategyID    integer primary key,"
        " Name          varchar(10))";

const QString DB_CONSTRUCTS_TABLE_SCHEMA =
        "create table Constructs"
        "(ConstructID   integer primary key,"
        " ExpireDate    date,"
        " ExpireDays    integer,"
        " StrikePrice   double,"
        " Type          varchar(4),"
        " Action        varChar(4),"
        " StrategyID    integer,"
        " FOREIGN KEY(StrategyID) REFERENCES Strategies(StrategyID))";

const QString DB_RULES_TABLE_SCHEMA =
        "create table Rules"
        "(RuleID        integer primary key,"
        " DeltaOperator varchar(1),"
        " DeltaValue    double,"
        " ThetaOperator varchar(1),"
        " ThetaValue    double,"
        " VolatilityOperator varchar(1),"
        " VolatilityValue    double,"
        " ExpireValue   double,"
        " Action        varchar(4),"
        " Execution     varchar(10),"
        " GracePeriod   integer,"
        " ConstructID   integer,"
        " FOREIGN KEY(ConstructID) REFERENCES Constructs(ConstructID))";

const QString DB_RELATION_TABLE_SCHEMA =
        "create table Relation"
        "(relID         integer primary key,"
        " cID           integer,"
        " rID           integer,"
        " FOREIGN KEY(cID) REFERENCES Constructs(ConstructID),"
        " FOREIGN KEY(rID) REFERENCES Rules(RulesID))";

//not in use atm. may use later
const QString DB_STOCK_TABLE_SCHEMA_OLD =
        "create table Stock"
        "(StockID       integer primary key,"
        " Name          varchar(10),"
        " Date          date,"
        " Open          double,"
        " High          double,"
        " Low           double,"
        " Settle        double,"
        " Volume        integer,"
        " InsertDate    date)";
/////////////////////////////////////////////

/////////////////////////////////////////////
///   Table Indexes
/////////////////////////////////////////////
const QString DB_OPTION_INDEX_DATE =
        "create index OPTION_INDEX_DATE "
        "on Option (Date)";

const QString DB_OPTION_INDEX_EXPDATE =
        "create index OPTION_INDEX_EXPDATE "
        "on Option (ExpireDate)";

const QString DB_STOCK_INDEX_DATE =
        "create index STOCK_INDEX_DATE "
        "on Stock (Date)";

/////////////////////////////////////////////

/////////////////////////////////////////////
///   Table column Metadata
///   Most of this data is temporary and will be changed before the final version
/////////////////////////////////////////////

const QString DATE_FORMAT = "yyyy'-'MM'-'dd";

struct TABLE_META{
    std::vector<int> colTarget;
    std::vector<int> colType; //excluding first and last (autonumber and insertdate)
    int size; //number of columns
    QString name;
    ~TABLE_META()
    {

    }
};

//0 = ignore, (1,2,...,n) = target DB column
const int optionColTarget[]   = {1,2,0,0,0,3,5,4,6,0,7,8,9,10,11,12,13,14,15,0}; //this should be created via the GUI
const int interestColTarget[] = {1,2,3,4,5,6,7,8,9,10,11};
const int stockColTarget[]    = {2,3,0,0};

//0 = number, 1 = string, 2 = date
const int optionColType[]     = {1,0,0,0,0,1,2,2,0,0,0,0,0, 0, 0, 0, 0, 0,0 ,0}; //changes what the field is surrounded with. References surround.
const int interestColType[]   = {2,0,0,0,0,0,0,0,0,0 ,0};
const int stockColType[]      = {2,0,0,0};


const TABLE_META DB_META[DB_TABLE_COUNT]
        = {{std::vector<int>(std::begin(optionColTarget),std::end(optionColTarget)),
            std::vector<int>(std::begin(optionColType), std::end(optionColType)),
            17, "Option"},
           {std::vector<int>(std::begin(interestColTarget),std::end(interestColTarget)),
            std::vector<int>(std::begin(interestColType), std::end(interestColType)),
            5, "Interest"},
           {std::vector<int>(std::begin(stockColTarget),std::end(stockColTarget)),
            std::vector<int>(std::begin(stockColType), std::end(stockColType)),
            4, "Stock"}};

const QString surround[][3] = {{" "," "},{"'","'"},{"'","'"}};
/////////////////////////////////////////////

/////////////////////////////////////////////
///   Structures
/////////////////////////////////////////////

struct OPTION{
    double strike;
    double underlying;
    double bid;
    double ask;
    QDate date;
    QDate expDate;
    QString type;
    int count;
    int delta;
    int theta;
};

/////////////////////////////////////////////

/////////////////
/// EXAMPLE CODE
/////////////////

const std::string exStringAr[] = {"2","w","asdasd"};
const std::vector<std::string> exampleStringVector(std::begin(exStringAr),std::end(exStringAr));

#endif // DATABASECONSTANTS_H
