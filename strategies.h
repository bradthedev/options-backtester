/*
 * Analysis Tool For Option Trading
 * Version 2.0
 *
 * Copyright 2014 Bradley Hunt & Benjamin Hall
 * Please contact Brad on bradhunt92@gmail.com for all your programming needs
 *
 * This file is part of "Analysis Tool For Option Trading",
 * aka "Analyis Tool", aka "Backtester".
 *
 * Analyis Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Analyis Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Analyis Tool.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef STRATEGIES_H
#define STRATEGIES_H

#include <QWidget>
#include <QDate>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

#include "database.h"
#include "qrystring.h"

namespace Ui {
class strategies;
}

class strategies : public QWidget
{
    Q_OBJECT

public:
    explicit strategies(QWidget *parent = 0);
    ~strategies();

    void openWindow();
    void updateComboStrike();
    void clearScreen();

    QDate startDate;
    Database db;

signals:
    void newStratCreated();

private slots:
    void on_backButton_clicked();

    void on_saveButton_clicked();

    void on_comboExpire_userDateChanged(const QDate &date);

    void on_expireDays_editingFinished();

    void on_addContract_clicked();

    void onStratSavedPressed();

private:
    Ui::strategies *ui;
};

#endif // STRATEGIES_H
